package handler

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/storage"
)

func (h *Handler) AddComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	trackID := vars["id"]

	j := GetJWTFromContext(r)

	if _, ok := j["userId"]; !ok {
		api.JSON(w, "Error no jwt")
		return
	}

	userID := j["userId"].(string)

	c := storage.Comment{}
	err := api.GetJSONContent(&c, r)

	err = h.Dao.InsertComment(c.Content, trackID, userID)

	if err != nil {
		fmt.Println(err)
	}

	w.WriteHeader(http.StatusCreated)
}
