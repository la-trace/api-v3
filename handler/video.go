package handler

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/la-trace/api-v3/api"
)

// GetUsers retrieve users
func (h *Handler) DeleteVideo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	trackID := vars["id"]
	err := h.Dao.DeleteVideo(trackID)
	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}
	api.JSONWithHTTPCode(w, nil, http.StatusNoContent)
}

// GetUser retrieve user
func (h *Handler) CreateVideo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	trackID := vars["id"]

	d := map[string]string{}
	err := api.GetJSONContent(&d, r)

	v, err := h.Dao.InsertVideo(d["platform"], d["slug"], trackID)

	switch {
	case err == sql.ErrNoRows:
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	case err != nil:
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	api.JSON(w, v)
}
