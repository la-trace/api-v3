# list of pkgs for the project without vendor
PKGS=$(shell go list ./... | grep -v /vendor/)
COMMIT_HASH=$(shell git log --format='%h' -n 1)
COMMIT_DATE=$(shell git log --format='%ad' --date=format:'%Y-%m-%d_%H:%M:%S' -n 1)

docker-build:
	@docker build -t alexjomin/la-trace-api .

dump:
	docker exec latraceapi_PostgreSQL_1 pg_dump -U postgres -F t la-trace | gzip >~/Google-Drive/backup-la-trace/la-trace.tar.gz

createdb:
	docker exec api-v3_PostgreSQL_1 createdb -U postgres la-trace

restore:
	docker cp dump api-v3_PostgreSQL_1:/tmp/.
	docker exec api-v3_PostgreSQL_1 bash -c "pg_restore -U postgres -d la-trace /tmp/dump"

dump:
	docker exec api-v3_PostgreSQL_1 bash -c "pg_dump -Fc --file=/tmp/dump --compress=9 la-trace"
	docker cp api-v3_PostgreSQL_1:/tmp/dump .

dump-schema:
	docker exec api-v3_PostgreSQL_1 pg_dump --schema-only -U postgres -F p la-trace > la-trace-schema.sql

start:
	sh -ac '. config/.env; \
	go run \
	-ldflags "-X gitlab.com/la-trace/api-v3/handler.GitRevision=${COMMIT_HASH} -X gitlab.com/la-trace/api-v3/handler.GitDate=${COMMIT_DATE}" \
	main.go'
