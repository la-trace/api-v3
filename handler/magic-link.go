package handler

import (
	"bytes"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/rs/xid"
	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/email"
	"gitlab.com/la-trace/api-v3/storage"
)

const (
	mailMagicLink        = "magic-link-fr.html"
	mailMagicLinkSubject = "Votre lien de connexion"
	// The duration after that the code is known as expiredg
	magicLinkExpiresAfter      = 2.0
	magicLinkEmailLogin        = "email-login"
	magicLinkEmailVerification = "email-verification"
)

func (h *Handler) createMagicLink(userID, action string) (code string, err error) {
	// Create a unique code
	guid := xid.New()
	code = guid.String()

	// Store in db
	return code, h.Dao.InsertMagicLink(code, userID, action)
}

func (h *Handler) sendLinkEmail(name, emailAddress, code string) error {
	m := email.Message{}

	var tpl bytes.Buffer

	tmpl, err := h.GetTemplate("email-login", "fr-FR")
	if err != nil {
		// TODO handling error in a better way
		panic(err)
	}

	data := struct {
		Link string
	}{
		Link: fmt.Sprintf("%s/fr/magic-link/%s", h.FrontHost, code),
	}

	err = tmpl.Execute(&tpl, data)
	if err != nil {
		// TODO handling error in a better way
		return err
	}

	m.HTMLContent = tpl.String()
	m.TextContent = mailMagicLinkSubject
	sender := email.NewContact("la-trace.com", "hello@la-trace.com")
	recipient := email.NewContact(name, emailAddress)
	m.Recipient = recipient
	m.Sender = sender
	m.Subject = mailMagicLinkSubject

	return h.EmailClient.Send(m)
}

func (h *Handler) AddMagicLink(w http.ResponseWriter, r *http.Request) {
	d := map[string]string{}
	err := api.GetJSONContent(&d, r)

	if _, ok := d["email"]; !ok {
		// email is not in payload
		// TODO Handle this error
		return
	}

	// GetUserID by email
	userID, err := h.Dao.GetUserIDByEmail(d["email"])

	if err != nil {
		log.Printf("db error: %s", err)
	}

	log.Printf("UserID: %s fouund for email", userID)

	if userID != "" {
		// Store in db
		code, err := h.createMagicLink(userID, magicLinkEmailLogin)
		if err != nil {
			log.Printf("Database error: %s", err)
		} else {
			// Send the email with the link
			h.sendLinkEmail("", d["email"], code)
		}
	}

	// Always returning a 204
	w.WriteHeader(http.StatusNoContent)
}

func (h *Handler) GetJWTFromMagicLing(w http.ResponseWriter, r *http.Request) {
	mr := storage.MagicLinkRequest{}
	err := api.GetJSONContent(&mr, r)

	if err != nil {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	m, err := h.Dao.GetMagicLinkByCode(mr.Code)

	switch {
	case err == sql.ErrNoRows:
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	case err != nil:
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	// THe code is not an email login
	if m.Action != magicLinkEmailLogin {
		fmt.Println("action", m.Action)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	// Check of the token
	duration := time.Since(m.CreatedAt)
	if duration.Minutes() > magicLinkExpiresAfter {
		fmt.Println("expiredLink", m.CreatedAt)
		api.JSONWithHTTPCode(w, errorBadCredential.payload(), errorBadCredential.code)
		return
	}

	jwt, err := forgeJWT(m.UserID, h.jwtKey)

	if err != nil {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	h.Dao.SetMagicLinkConsumed(m.Code)

	api.JSON(w, map[string]string{"token": jwt})
}
