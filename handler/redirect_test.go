package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

func TestRedirectHandler(t *testing.T) {

	h := HandlerFactory()
	h.FrontHost = "http://foo.bar"

	// Hack to try to fake gorilla/mux vars
	// Both name and id are needed
	vars := map[string]string{
		"name": "test",
		"id":   "test",
	}

	req, err := http.NewRequest(http.MethodGet, "/foo", nil)

	if err != nil {
		t.Fatal(err)
	}

	req = mux.SetURLVars(req, vars)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(h.Redirect)
	handler.ServeHTTP(rr, req)

	// Handler should return a http.StatusMovedPermanently code (aka 301)
	if status := rr.Code; status != http.StatusMovedPermanently {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusCreated)
	}

	// Header Location should be correct
	wantedURL := "http://foo.bar/fr/track/8229f9ff-ac00-4ad2-84fc-3cf7d73ad0eb"
	location := rr.HeaderMap.Get("location")
	if location != wantedURL {
		t.Errorf("Redirect url not correct:  got %v want %v", location, wantedURL)
	}
}
