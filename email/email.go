package email

import (
	"fmt"

	sendgrid "github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type Contact struct {
	Name  string
	Email string
}

type Message struct {
	Subject     string
	TextContent string
	HTMLContent string
	Recipient   Contact
	Sender      Contact
}

var from = Contact{
	Name:  "la-trace.com",
	Email: "hello@la-trace.com",
}

type Provider struct {
	client *sendgrid.Client
}

func NewContact(name, email string) Contact {
	return Contact{
		Name:  name,
		Email: email,
	}
}

func (s *Provider) Send(m Message) error {
	from := mail.NewEmail(m.Sender.Name, m.Sender.Email)
	to := mail.NewEmail(m.Recipient.Name, m.Recipient.Email)

	// a := mail.NewASM()
	// a.SetGroupID(6411)
	// a.AddGroupsToDisplay(6411)

	message := mail.NewSingleEmail(from, m.Subject, to, m.TextContent, m.HTMLContent)
	// message.SetASM(a)

	r, err := s.client.Send(message)
	fmt.Printf("%v", r)
	return err
}

func New(apiKey string) Provider {
	s := Provider{
		client: sendgrid.NewSendClient(apiKey),
	}
	return s
}
