package handler

import (
	"bytes"
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/email"
	"gitlab.com/la-trace/api-v3/storage"
	validator "gopkg.in/go-playground/validator.v9"
)

const (
	mailNewAccount        = "new-user-fr.html"
	mailNewAccountSubject = "Votre compte"
)

func (h *Handler) AddUser(w http.ResponseWriter, r *http.Request) {
	u := storage.AddingUser{}
	err := api.GetJSONContent(&u, r)

	err = h.Validator.Struct(&u)
	fields := map[string]string{}

	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			fields[err.Field()] = err.ActualTag()
		}
		api.UnprocessableError(w, fields)
		return
	}

	usr, err := h.Dao.AddUser(u)
	if err != nil {
		if err == storage.ErrEmailAlreadyExists {
			api.Error(w, "Account already exists", http.StatusConflict)
			return
		}
	}

	var tpl bytes.Buffer

	tmpl, err := h.GetTemplate("welcome", "fr-FR")
	if err != nil {
		// TODO handling error in a better way
		panic(err)
	}

	// Prepare the link to verify the email address
	code, err := h.createMagicLink(usr.ID, magicLinkEmailVerification)

	data := struct {
		Link string
	}{
		Link: fmt.Sprintf("%s/fr/verify-email/%s", h.FrontHost, code),
	}

	err = tmpl.Execute(&tpl, data)
	if err != nil {
		// TODO handling error in a better way
		panic(err)
	}

	m := email.Message{}
	m.HTMLContent = tpl.String()
	m.TextContent = mailNewAccountSubject
	sender := email.NewContact("la-trace.com", "hello@la-trace.com")
	recipient := email.NewContact(u.Name, u.Email)

	m.Recipient = recipient
	m.Sender = sender
	m.Subject = mailNewAccountSubject

	err = h.EmailClient.Send(m)

	api.JSON(w, usr)
}

// // GetUser retrieve user
// func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {

// 	user := storage.User{}

// 	err := h.DB.
// 		QueryRow(`SELECT id, name FROM public.user`).
// 		Scan(&user.ID, &user.Name)

// 	switch {
// 	case err == sql.ErrNoRows:
// 		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
// 		return
// 	case err != nil:
// 		log.Printf("Database error: %s", err)
// 		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
// 		return
// 	}

// 	api.JSON(w, user)

// }

// GetUser retrieve user
func (h *Handler) GetMe(w http.ResponseWriter, r *http.Request) {

	user := storage.UserLight{}

	j := GetJWTFromContext(r)

	if _, ok := j["userId"]; !ok {
		api.JSON(w, "Error no jwt")
		return
	}

	err := h.DB.
		QueryRow(`SELECT id, name, email, creation FROM public.user WHERE id = $1`, j["userId"]).
		Scan(&user.ID, &user.Name, &user.Email, &user.Creation)

	switch {
	case err == sql.ErrNoRows:
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	case err != nil:
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	api.JSON(w, user)

}

// GetUser retrieve user
func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	userID := vars["userID"]
	user := storage.UserLight{}

	err := h.DB.
		QueryRow(`SELECT id, name, email, creation FROM public.user WHERE id = $1`, userID).
		Scan(&user.ID, &user.Name, &user.Email, &user.Creation)

	switch {
	case err == sql.ErrNoRows:
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	case err != nil:
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	api.JSON(w, user)

}

func (h *Handler) UpdatePassword(w http.ResponseWriter, r *http.Request) {
	p := storage.EditablePassword{}
	err := api.GetJSONContent(&p, r)
	err = h.Validator.Struct(&p)
	fields := map[string]string{}

	j := GetJWTFromContext(r)

	if _, ok := j["userId"]; !ok {
		api.JSON(w, "Error no jwt")
		return
	}

	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			fields[err.Field()] = err.ActualTag()
		}
		api.UnprocessableError(w, fields)
		return
	}

	err = h.Dao.UpdatePassword(p.Password, j["userId"].(string))
	if err != nil {
		fmt.Println(err)
		api.Error(w, "Update Error", http.StatusInternalServerError)
		return

	}

	w.WriteHeader(http.StatusNoContent)
}

func (h *Handler) PatchUser(w http.ResponseWriter, r *http.Request) {
	j := GetJWTFromContext(r)

	var id string

	if _, ok := j["userId"]; !ok {
		api.JSON(w, "Error no jwt")
		return
	}

	id = j["userId"].(string)

	t := storage.EditableUser{}
	err := api.GetJSONContent(&t, r)

	if err != nil {
	}

	err = h.Dao.PatchUser(id, t)

	if err != nil {
		fmt.Println(err)
	}

	w.WriteHeader(http.StatusNoContent)
}

func (h *Handler) VerifyEmail(w http.ResponseWriter, r *http.Request) {
	mr := storage.MagicLinkRequest{}
	err := api.GetJSONContent(&mr, r)

	if err != nil {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	m, err := h.Dao.GetMagicLinkByCode(mr.Code)

	switch {
	case err == sql.ErrNoRows:
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	case err != nil:
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	// THe code is not an email login
	if m.Action != magicLinkEmailVerification {
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	err = h.Dao.SetEmailValidated(m.Code, m.UserID)

	if err != nil {
		fmt.Println("error", err)
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	h.Dao.SetMagicLinkConsumed(m.Code)

	// Always returning a 204
	w.WriteHeader(http.StatusNoContent)

}

func (h *Handler) MyTracks(w http.ResponseWriter, r *http.Request) {
	limit := r.URL.Query().Get("limit")

	j := GetJWTFromContext(r)
	var id string
	if _, ok := j["userId"]; !ok {
		api.JSON(w, "Error no jwt")
		return
	}
	id = j["userId"].(string)

	if limit == "" {
		limit = "20"
	}

	offset := r.URL.Query().Get("offset")

	if offset == "" {
		offset = "0"
	}

	rows, err := h.DB.Query(`
		SELECT
			t.id, t.name, t.description, t.elevation_gain, t.elevation_loss, t.creation, t.distance,
			u.id, u.name, u.creation, u.modification,
			count(*) OVER() AS full_count
		FROM
			public.track t
		LEFT JOIN
			"user" u
		ON
			u.id = t.user_id
		WHERE
			t.user_id = $1
		ORDER BY t.creation DESC
		LIMIT $2 OFFSET $3`, id, limit, offset)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	}

	tracks := []storage.Track{}
	count := ""

	for rows.Next() {
		t := storage.Track{}
		rows.Scan(&t.ID, &t.Name, &t.Description, &t.ElevationGain, &t.ElevationLoss, &t.Creation, &t.Distance, &t.User.ID, &t.User.Name, &t.User.Creation, &t.User.Modification, &count)
		tracks = append(tracks, t)
	}

	w.Header().Set("X-Total-Count", count)
	w.Header().Set("Access-Control-Expose-Headers", "X-Total-Count")

	api.JSON(w, tracks)
}

func (h *Handler) Profile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["userID"]

	limit := r.URL.Query().Get("limit")
	if limit == "" {
		limit = "20"
	}

	offset := r.URL.Query().Get("offset")
	if offset == "" {
		offset = "0"
	}

	tracks, count, err := h.Dao.GetTracksByUserID(userID, limit, offset)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	}

	w.Header().Set("X-Total-Count", count)
	w.Header().Set("Access-Control-Expose-Headers", "X-Total-Count")

	api.JSON(w, tracks)
}
