
-- +migrate Up
CREATE TYPE external_provider AS ENUM ('strava');

ALTER TABLE track ADD COLUMN provider_name external_provider;
ALTER TABLE track ADD COLUMN provider_id VARCHAR;

-- +migrate Down
ALTER TABLE track DROP COLUMN provider_name;
ALTER TABLE track DROP COLUMN provider_id;
DROP TYPE external_provider;
