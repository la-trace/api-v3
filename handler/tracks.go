package handler

import (
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/model"
	"gitlab.com/la-trace/api-v3/storage"
)

const (
	MimeGPX     = "application/gpx+xml"
	MimeGeoJSON = "application/vnd.geo+json"
)

// GetTrack Returns a Track
func (h *Handler) GetTrack(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	t := &storage.Track{}
	videos := []storage.Video{}
	pix := []storage.Picture{}
	comments := []storage.Comment{}
	pois := []storage.Poi{}

	t, err := h.Dao.GetTrackByID(id)

	switch {
	case err == storage.ErrNoRows:
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	case err != nil:
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	}

	pix, _ = h.Dao.GetPicturesByTrackID(id)
	videos, _ = h.Dao.GetVideosByTrackID(id)
	comments, _ = h.Dao.GetCommentsByTrackID(id)
	pois, _ = h.Dao.GetPoisByTrackID(id)

	if pix != nil {
		t.Pictures = pix
	}

	if videos != nil {
		t.Videos = videos
	}

	if comments != nil {
		t.Comments = comments
	}

	if pois != nil {
		t.Pois = pois
	}

	// render JSON
	api.JSON(w, t)
}

func (h *Handler) PatchTrack(w http.ResponseWriter, r *http.Request) {
	//@TODO check ownership of the track
	vars := mux.Vars(r)
	id := vars["id"]

	t := storage.EditableTrack{}
	err := api.GetJSONContent(&t, r)

	if err != nil {
	}

	err = h.Dao.PatchTrack(id, t)

	if err != nil {
		fmt.Println(err)
	}

	api.JSON(w, t)
}

// @openapi:path
// /track/{trackId}:
//	get:
//		tags:
//		- track
//		summary: Retrieve a track
//		description: Returns a track corresponding to specified id
//		parameters:
//			- in: header
//				name: Accept
//				schema:
//					type: string
//					enum: [application/json, application/vnd.geo+json]
//				description: The type of content to be returned, json or geojson
//			- in: path
//				name: trackId
//				schema:
//					type: string
//				required: true
//				description: uuid of the track
//		responses:
//			"200":
//				description: "A Track"
//				content:
//					application/json:
//						schema:
//							$ref: "#/components/schemas/Track"
//					application/vnd.geo+json:
//						schema:
//							$ref: "#/components/schemas/Geojson"
//			"404":
//				description: "Track not found"
//				content:
//					application/json:
//						schema:
//							$ref: "#/components/schemas/Error"
func (h *Handler) GetTrackRouting(w http.ResponseWriter, r *http.Request) {
	header := r.Header.Get("Accept")
	switch header {
	case MimeGeoJSON:
		h.GetTrackAsGeometry(w, r, "geojson")
	default:
		h.GetTrack(w, r)
	}
}

func (h *Handler) GetGPX(w http.ResponseWriter, r *http.Request) {
	header := r.Header.Get("Accept")
	switch header {
	case MimeGPX:
		h.GetTrackAsGeometry(w, r, "gpx")
	default:
	}
}

func (h *Handler) GetTrackAsGeometry(w http.ResponseWriter, r *http.Request, output string) {
	vars := mux.Vars(r)
	trackID := vars["id"]

	// Simplifiction factor for Douglas Peucker Algorythm
	f, err := strconv.ParseFloat(r.URL.Query().Get("factor"), 64)

	if err != nil || f < 0 || f > 1 {
		f = 0
	}

	geometry, err := h.Dao.GetTrackAsGeoJSONByIDAndFactor(trackID, f)

	if output == "gpx" {

		t, err := h.Dao.GetTrackByID(trackID)
		if err != nil {
			api.Error(w, "Error whil retrieving track", http.StatusInternalServerError)
		}
		j := GetJWTFromContext(r)
		if _, ok := j["userId"]; !ok {
			api.JSON(w, "Error no jwt")
			fmt.Println("error in download")
			return
		}
		userID := j["userId"].(string)
		err = h.Dao.LogDownload(userID, trackID)

		gpx := model.NewGpx()
		gpx.Tracks[0].Name = t.Name
		h.Dao.IncNumberOfDownloadsOfTrack(trackID)
		for _, pos := range geometry.LineString {
			tw := model.Wpt{}
			tw.Lon = pos[0]
			tw.Lat = pos[1]
			tw.Ele = pos[2]
			gpx.Tracks[0].Segments[0].Waypoints = append(gpx.Tracks[0].Segments[0].Waypoints, tw)
		}
		w.Write(gpx.ToXML())
	} else {
		api.JSON(w, geometry)
	}
}

func (h *Handler) DeleteTrack(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	err := h.Dao.DeleteTrack(id)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}
	api.JSONWithHTTPCode(w, nil, http.StatusNoContent)
}

// GetNearTracksAsGeoJSON
// @openapi:path
// /tracks/near/{lon}/{lat}:
//	get:
//		tags:
//		- track
//		summary: Retrieve tracks near a point
//		description: Returns a tracks near a point within a radius of 10km, sorted by proximity
//		parameters:
// 			- in: path
//				name: lat
//				schema:
//					type: string
//				required: true
//				description: the latitude of the point
//			- in: path
//				name: lon
//				schema:
//					type: string
//				required: true
//				description: the longitude of the point
//		responses:
//			"200":
//				description: "An Array of Tracks"
//				content:
//					application/json:
//						schema:
//							type: array
//							items:
//								$ref: "#/components/schemas/Track"
//					application/vnd.geo+json:
//						schema:
//							$ref: "#/components/schemas/Geojson"
func (h *Handler) GetNearTracksAsGeoJSON(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	lonS := vars["lon"]
	latS := vars["lat"]

	lon, _ := strconv.ParseFloat(lonS, 64)
	lat, _ := strconv.ParseFloat(latS, 64)

	geometries, err := h.Dao.GetTrackIDNearLonLat(lon, lat)

	switch {
	case err == sql.ErrNoRows:
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	case err != nil:
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	api.JSON(w, geometries)
}

func (h *Handler) GetIntersectsTracksAsCentroidGeoJSON(w http.ResponseWriter, r *http.Request) {
	swLonS := r.URL.Query().Get("sw_lon")
	swLatS := r.URL.Query().Get("sw_lat")
	neLonS := r.URL.Query().Get("ne_lon")
	neLatS := r.URL.Query().Get("ne_lat")

	q := formatFilterAsQuery(r.URL.RawQuery)

	if q != "" {
		q = " AND " + q
		fmt.Println("filter : " + q)
	}

	if swLonS == "" || swLatS == "" || neLonS == "" || neLatS == "" {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	swLon, _ := strconv.ParseFloat(swLonS, 64)
	swLat, _ := strconv.ParseFloat(swLatS, 64)
	neLon, _ := strconv.ParseFloat(neLonS, 64)
	neLat, _ := strconv.ParseFloat(neLatS, 64)

	p, err := h.Dao.GetIntersectsTracksAsCentroidGeoJSON(swLon, swLat, neLon, neLat, q)

	if err != nil {
		fmt.Println(err)
	}

	api.JSON(w, p)
}

func (h *Handler) GetIntersectsTracksAsGeoJSON(w http.ResponseWriter, r *http.Request) {
	swLonS := r.URL.Query().Get("sw_lon")
	swLatS := r.URL.Query().Get("sw_lat")
	neLonS := r.URL.Query().Get("ne_lon")
	neLatS := r.URL.Query().Get("ne_lat")

	if swLonS == "" || swLatS == "" || neLonS == "" || neLatS == "" {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	swLon, _ := strconv.ParseFloat(swLonS, 64)
	swLat, _ := strconv.ParseFloat(swLatS, 64)
	neLon, _ := strconv.ParseFloat(neLonS, 64)
	neLat, _ := strconv.ParseFloat(neLatS, 64)

	q := formatFilterAsQuery(r.URL.RawQuery)

	if q != "" {
		q = " AND " + q
		fmt.Println("filter : " + q)
	}

	ids, err := h.Dao.GetIntersectingTracksId(swLon, swLat, neLon, neLat, q)

	if err != nil {
		fmt.Println(err)
	}

	api.JSON(w, ids)
}

func (h *Handler) GetElevations(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	results, _ := h.Dao.GetTrackElevationByID(id)
	api.JSON(w, results)
}
