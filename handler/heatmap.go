package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/umahmood/haversine"
	"gitlab.com/la-trace/api-v3/api"
)

const heatmapDistanceLimit = 20

func (h *Handler) Heatmap(w http.ResponseWriter, r *http.Request) {
	swLonS := r.URL.Query().Get("sw_lon")
	swLatS := r.URL.Query().Get("sw_lat")
	neLonS := r.URL.Query().Get("ne_lon")
	neLatS := r.URL.Query().Get("ne_lat")

	swLon, _ := strconv.ParseFloat(swLonS, 64)
	swLat, _ := strconv.ParseFloat(swLatS, 64)
	neLon, _ := strconv.ParseFloat(neLonS, 64)
	neLat, _ := strconv.ParseFloat(neLatS, 64)

	// Check that the bounding box is not too wide
	// Could overload the database as we are dumping the whole
	// poits in the the bounding box
	pt1 := haversine.Coord{Lat: swLat, Lon: swLon}
	pt2 := haversine.Coord{Lat: neLat, Lon: swLon}
	pt3 := haversine.Coord{Lat: neLat, Lon: neLon}

	_, d1 := haversine.Distance(pt1, pt2)
	_, d2 := haversine.Distance(pt2, pt3)

	if d1 > heatmapDistanceLimit || d2 > heatmapDistanceLimit {
		w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
		return
	}

	p, err := h.Dao.GetHeatMap(swLon, swLat, neLon, neLat)

	if err != nil {
		fmt.Println(err)
	}

	api.JSON(w, p)
}

func (h *Handler) UserHeatmap(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["userID"]

	p, err := h.Dao.GetUserHeatMap(userID)
	if err != nil {
		fmt.Println(err)
	}
	api.JSON(w, p)
}
