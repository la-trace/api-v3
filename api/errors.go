package api

import (
	"net/http"
)

type validationError struct {
	Field   string `json:"field"`
	Code    string `json:"code"`
	Message string `json:"message"`
}

func Error(w http.ResponseWriter, message string, code int) {
	payload := ApiError{
		Message: message,
	}
	JSONWithHTTPCode(w, payload, code)
}

func UnprocessableError(w http.ResponseWriter, kv map[string]string) {
	ve := []validationError{}
	for field, rule := range kv {
		v := validationError{
			Field:   field,
			Message: getErrorMessage(rule),
		}
		ve = append(ve, v)
	}
	payload := UnprocessableEntityError{
		Message: "Request payload validation failed",
		Errors:  ve,
	}
	JSONWithHTTPCode(w, payload, http.StatusUnprocessableEntity)
}

func getErrorMessage(rule string) string {
	switch rule {
	case "email":
		return "not a valid email address"
	case "required":
		return "can not be empty"
	default:
		return "is not correct"
	}
}

type ApiError struct {
	Message string `json:"message"`
}

type UnprocessableEntityError struct {
	Errors  []validationError `json:"errors"`
	Message string            `json:"message"`
}
