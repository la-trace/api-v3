-- name: getTrack
SELECT
	t.id, t.name, t.description,
	COALESCE(t.elevation_gain, 0),
	COALESCE(t.elevation_loss, 0),
	t.creation, COALESCE(t.distance, 0), t.start_point,
	t.end_point, t.rating, COALESCE(t.Downloads, 0), u.id, u.name,
	u.creation, u.modification
FROM track t
LEFT JOIN "user" u
ON u.id = t.user_id
WHERE t.id = $1

-- name: getTrackAsGeoJSON
SELECT ST_AsGeoJSON(ST_Simplify(linestring::geometry, $1))
FROM track
WHERE id = $2

-- name: getPictures
SELECT
	p.id, p.creation, p.caption, p.width, p.height, COALESCE(p.file, id || file_extension), ST_AsGeoJSON(p.position)
FROM picture p
WHERE p.track_id = $1
ORDER BY creation ASC

-- name: getVideos
SELECT v.id, v.creation, v.modification, v.platform, v.slug
FROM video v
WHERE v.track_id = $1

-- name: getComments
SELECT
	c.id, c.creation, c.modification, c.content,
	u.id, u.name, u.creation, u.modification
FROM
	comment c
LEFT JOIN
	"user" u
ON
	u.id = c.user_id
WHERE
	c.track_id = $1
ORDER BY
	c.creation

-- name: getPois
SELECT p.id, p.caption, modification, creation, COALESCE(icon, ''), ST_AsGeoJSON(p.position)
FROM poi p
WHERE p.track_id = $1
ORDER BY creation ASC

-- name: getNearLonLat
SELECT
	ST_Distance(linestring, ST_MakePoint($1,$2)) AS dist,
	id,
	name,
	description
FROM public.track
WHERE
	ST_DWithin(linestring, ST_MakePoint($1,$2), 15000)
ORDER BY
	dist ASC
LIMIT $3

-- name: getElevation
SELECT ROUND(elevation) as elevation, ROUND( COALESCE( SUM( ST_Distance(current::geography, previous::geography)) over (rows unbounded preceding),0)) as distance, ST_X(current) as lon, ST_Y(current) as lat FROM(
	SELECT
		ST_Z((dump).geom) AS elevation,
		(dump).geom AS current,
		lag ((dump).geom, 1) OVER (ORDER BY (dump).path ASC) AS previous FROM (
			SELECT 	(ST_DumpPoints(ST_Simplify(linestring::geometry, 0.0001))) as dump FROM track WHERE id = $1
		) as tbl
) as t

-- name: getIntersectingTracksAsGeoJSONPoint
SELECT
	track.id AS track_id,
	ST_AsGeoJSON(ST_Centroid(track.linestring::geometry))
FROM track
WHERE
	ST_Intersects(
		track.linestring,
		ST_MakeEnvelope($1, $2, $3, $4)
	)

-- name: getIntersectingTracksId
SELECT id
FROM track
WHERE
	ST_Intersects(
		linestring,
		ST_MakeEnvelope($1, $2, $3, $4, 4326)
	)

-- name: login
SELECT id, name, id_mysql, validation_id
FROM public.user
WHERE email = $1 AND password = ENCODE(digest($2, 'sha512'), 'hex')

-- name: insertPicture
INSERT INTO
public.picture (width, height, position, track_id, file_extension)
VALUES
	($1, $2,
	CASE
		WHEN $3 <> 0.0 AND $4 <> 0.0 THEN ST_Point($3, $4) ELSE null
	END, $5, $6)
RETURNING id

-- name: deletePicture
DELETE FROM picture WHERE id = $1
RETURNING CONCAT(id, file_extension)

-- name: insertPoi
INSERT INTO poi (caption, track_id, position)
VALUES ($1, $2, ST_Point($3, $4))
RETURNING id

-- name: deletePoi
DELETE FROM poi WHERE id = $1

-- name: insertVideo
INSERT INTO video (platform, slug, track_id)
VALUES ($1, $2, $3)
RETURNING id

-- name: deleteVideo
DELETE FROM video WHERE track_id = $1

-- name: patchTrack
UPDATE track SET (%s) = ROW(%s) WHERE id = $%d

-- name: patchPoi
UPDATE poi SET (%s) = ROW(%s) WHERE id = $%d

-- name: patchPictures
UPDATE picture SET (%s) = ROW(%s) WHERE id = $%d

-- name: patchUser
UPDATE public.user SET (%s) = ROW(%s) WHERE id = $%d

-- name: deleteTrack
DELETE FROM track WHERE id = $1

-- name: addUser
INSERT INTO "user" (name, email, password)
VALUES ($1, $2, ENCODE(digest($3, 'sha512'), 'hex'))
RETURNING id, name, creation

-- name: incDownload
UPDATE track SET downloads = downloads + 1 WHERE id = $1 

-- name: getStarred
SELECT DISTINCT ON (t.downloads) t.id, t.distance, t.elevation_gain, t.name, p.file, t.start_point from track t
INNER JOIN picture p on p.track_id = t.id
ORDER BY t.downloads DESC
LIMIT 20

-- name: quickSearch
SELECT t.id, t.name
FROM public.track t
WHERE textsearchable @@ plainto_tsquery('french', $1)
LIMIT $2

-- name: updatePassword
UPDATE public.user SET password = ENCODE(digest($1, 'sha512'), 'hex') WHERE id = $2

-- name: addComment
INSERT INTO comment (content, track_id, user_id) VALUES ($1, $2, $3)

-- name: addMagicLink
INSERT INTO magic_link (code, user_id, action) VALUES ($1, $2, $3)

-- name: getMagicLinkByCode
SELECT code, created_at, consumed_at, user_id, code, action FROM magic_link WHERE code = $1 AND consumed_at IS NULL

-- name: setMagicLinkConsumed
UPDATE magic_link SET consumed_at = NOW() WHERE code = $1

-- name: getUserIdByEmail
SELECT id as count FROM public.user WHERE email = $1

-- name: validateEmailByUserID
UPDATE public.user SET validation_id = $1 WHERE id = $2

-- name: logDowload
INSERT INTO download (user_id, track_id) VALUES ($1, $2)

-- name: getAlltrackID
SELECT id FROM track

-- name: heatmap
WITH points as (
SELECT
	ST_X((ST_DumpPoints(linestring::geometry)).geom) as x,
	ST_Y((ST_DumpPoints(linestring::geometry)).geom) as y,
	COUNT(*) AS c
FROM track
WHERE 	
	ST_Intersects(linestring::geometry, ST_MakeEnvelope($1, $2, $3, $4, 4326))
GROUP BY 
	x, y
)
SELECT y, x, TRUNC(c::numeric / (SELECT MAX(c) FROM points), 3) as w FROM points
WHERE 
	ST_Intersects(
		ST_SetSRID(ST_POINT(x, y), 4326),
		ST_MakeEnvelope($1, $2, $3, $4, 4326))

-- name: getIdFromMySQLID
SELECT id FROM track WHERE id_mysql = $1

-- name: getTiles
SELECT ST_AsMVT(q, 'gr', 4096, 'geom')
FROM (
	SELECT 
		id, 
		filename,
		ST_AsMvtGeom(
			ST_Transform(ST_SetSRID(linestring, 4326), 3857),
			BBox($1, $2, $3),
			4096,
			256,
			false
		) AS geom
	FROM gr
	WHERE ST_Intersects(ST_Transform(ST_SetSRID(linestring, 4326), 3857), BBox($1, $2, $3))
) AS q

-- name: getPostGisVersion
SELECT PostGIS_full_version() 

-- name: userHeatmap
WITH points AS (
SELECT
	COUNT(*) as c,
	TRUNC(ST_X(ST_StartPoint(linestring::geometry))::numeric, 4) as x,
	TRUNC(ST_Y(ST_StartPoint(linestring::geometry))::numeric, 4) as y
FROM track
WHERE user_id = $1
GROUP BY x, y
)
SELECT y, x, TRUNC(c::numeric / (select SUM(c) FROM points), 3) as w FROM points

-- name: getTracksByUserID
SELECT
	t.id, t.name, t.description, t.elevation_gain, t.elevation_loss, t.creation, t.distance, t.downloads,
	u.id, u.name, u.creation, u.modification,
	count(*) OVER() AS full_count
FROM
	public.track t
LEFT JOIN
	"user" u
ON
	u.id = t.user_id
WHERE
	t.user_id = $1
ORDER BY t.creation DESC
LIMIT $2 OFFSET $3