package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	negronilogrus "github.com/meatballhat/negroni-logrus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/urfave/negroni"
	"gitlab.com/la-trace/api-v3/connector/strava"
	"gitlab.com/la-trace/api-v3/handler"
	"gitlab.com/la-trace/api-v3/storage/postgres"
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "la-trace-api",
	Short: "Rest API of la-trace.com",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		n := negroni.New()
		n.Use(negronilogrus.NewMiddlewareFromLogger(logrus.StandardLogger(), "la-trace"))

		dao, err := postgres.New(conf.PG, conf.Queries, conf.PasswordSalt)

		check, err := dao.CheckPostGisVersion()
		if err != nil {
			log.Fatal(err)
		}
		if check == false {
			log.Fatal("Minimal POSTGis needed version is 2.4.x")
		}

		hh, err := handler.NewHandler(conf.PG, conf.Pictures, conf.SendGridKey, conf.FrontHost, dao, conf.JWTkey, conf.RenderGeoJSONURL)
		if err != nil {
			log.Fatal(err)
		}

		err = hh.RegisterTemplate("welcome", "fr-FR", conf.EmailTemplates+"/new-user-fr-FR.html")
		if err != nil {
			log.Fatal(err)
		}

		err = hh.RegisterTemplate("email-login", "fr-FR", conf.EmailTemplates+"/email-login-fr-FR.html")
		if err != nil {
			log.Fatal(err)
		}

		s := strava.New(conf.StravaClientID, conf.StravaClientSecret, conf.StravaRedirectURL)

		hh.PlayMigration(conf.Migrations)

		r := mux.NewRouter()

		jwtKey := conf.JWTkey

		// Users
		r.HandleFunc("/users/me/password", handler.CheckAuth(jwtKey, hh.UpdatePassword)).Methods(http.MethodPut)
		r.HandleFunc("/users/me", handler.CheckAuth(jwtKey, hh.GetMe)).Methods(http.MethodGet)
		r.HandleFunc("/users/{userID}", hh.GetUser).Methods(http.MethodGet)
		r.HandleFunc("/users/me/tracks", handler.CheckAuth(jwtKey, hh.MyTracks)).Methods(http.MethodGet)
		r.HandleFunc("/users/{userID}/tracks", hh.Profile).Methods(http.MethodGet)
		r.HandleFunc("/users/me", handler.CheckAuth(jwtKey, hh.PatchUser)).Methods(http.MethodPatch)
		r.HandleFunc("/register", hh.AddUser).Methods(http.MethodPost)

		r.HandleFunc("/contact", hh.Contact).Methods(http.MethodPost)

		// Login
		r.HandleFunc("/login", hh.Login).Methods(http.MethodPost)
		r.HandleFunc("/refresh", hh.Refresh).Methods(http.MethodGet)

		// Search
		r.HandleFunc("/search", hh.Search).Methods(http.MethodGet)
		r.HandleFunc("/last", hh.Last).Methods(http.MethodGet)
		r.HandleFunc("/tracks/intersects", hh.GetIntersectsTracksAsGeoJSON).Methods(http.MethodGet)
		r.HandleFunc("/tracks/intersects/centroid", hh.GetIntersectsTracksAsCentroidGeoJSON).Methods(http.MethodGet)
		r.HandleFunc("/tracks/near/{lon}/{lat}", hh.GetNearTracksAsGeoJSON).Methods(http.MethodGet)

		r.HandleFunc("/tracks/starred", hh.GetStarred).Methods(http.MethodGet)

		// Track
		r.HandleFunc("/tracks/{id}/gpx", handler.CheckAuth(jwtKey, hh.GetGPX)).Methods(http.MethodGet)
		r.HandleFunc("/tracks/{id}", hh.GetTrackRouting).Methods(http.MethodGet)
		r.HandleFunc("/tracks/{id}", handler.CheckAuth(jwtKey, hh.DeleteTrack)).Methods(http.MethodDelete)
		r.HandleFunc("/tracks/{id}", handler.CheckAuth(jwtKey, hh.PatchTrack)).Methods(http.MethodPatch)
		r.HandleFunc("/tracks/{id}/elevations", hh.GetElevations).Methods(http.MethodGet)

		// Comments
		r.HandleFunc("/tracks/{id}/comments", handler.CheckAuth(jwtKey, hh.AddComment)).Methods(http.MethodPost)

		// Pois
		r.HandleFunc("/pois/{id}", handler.CheckAuth(jwtKey, hh.PatchPoi)).Methods(http.MethodPatch)
		r.HandleFunc("/pois/{id}", handler.CheckAuth(jwtKey, hh.DeletePoi)).Methods(http.MethodDelete)
		r.HandleFunc("/tracks/{id}/pois", handler.CheckAuth(jwtKey, hh.CreatePoi)).Methods(http.MethodPost)

		// Videos
		r.HandleFunc("/tracks/{id}/video", handler.CheckAuth(jwtKey, hh.CreateVideo)).Methods(http.MethodPost)
		r.HandleFunc("/tracks/{id}/video", handler.CheckAuth(jwtKey, hh.DeleteVideo)).Methods(http.MethodDelete)

		// Pictures
		r.HandleFunc("/tracks/{id}/pictures", handler.CheckAuth(jwtKey, hh.CreatePictures)).Methods(http.MethodPost)
		r.HandleFunc("/pictures/{id}", handler.CheckAuth(jwtKey, hh.DeletePictures)).Methods(http.MethodDelete)
		r.HandleFunc("/pictures/{id}", handler.CheckAuth(jwtKey, hh.PatchPictures)).Methods(http.MethodPatch)

		// Magic Links
		r.HandleFunc("/magic-link", hh.AddMagicLink).Methods(http.MethodPost)
		r.HandleFunc("/jwt", hh.GetJWTFromMagicLing).Methods(http.MethodPost)

		// Email validation
		r.HandleFunc("/email-validation", hh.VerifyEmail).Methods(http.MethodPost)

		// Strava
		r.HandleFunc("/strava/connect", s.Connect).Methods(http.MethodGet)
		r.HandleFunc("/strava/callback", s.Callback).Methods(http.MethodGet)

		// GPX Upload
		r.HandleFunc("/upload", handler.CheckAuth(jwtKey, hh.Upload)).Methods(http.MethodPost)

		// Auth delegation
		r.HandleFunc("/auth", handler.CheckAuth(jwtKey, hh.Auth)).Methods(http.MethodGet)

		// Metrics
		r.Handle("/metrics", promhttp.Handler()).Methods(http.MethodGet)

		// Revision
		r.HandleFunc("/revision", hh.Revision).Methods(http.MethodGet)

		// Sitemap
		r.HandleFunc("/sitemap", hh.Sitemap).Methods(http.MethodGet)

		// Heatmap
		r.HandleFunc("/heatmap", hh.Heatmap).Methods(http.MethodGet)
		r.HandleFunc("/users/{userID}/heatmap", hh.UserHeatmap).Methods(http.MethodGet)

		// Render Geojson
		r.HandleFunc("/render/{trackID}", hh.Render).Methods(http.MethodGet)

		// V2 Redirection
		r.HandleFunc("/itineraires/vtt/{id}/{name}", hh.Redirect).Methods(http.MethodGet)
		r.HandleFunc("/itineraires/rando-trail/{id}/{name}", hh.Redirect).Methods(http.MethodGet)

		// Vector Tiles
		// GR (https://fr.wikipedia.org/wiki/Sentier_de_grande_randonn%C3%A9e)
		r.HandleFunc("/vector-tiles/gr/{z}/{x}/{y}.mvt", hh.GRTiles).Methods(http.MethodGet)

		// add middleware for CORS
		cors := cors.New(cors.Options{
			AllowedHeaders:   []string{"*"},
			AllowedMethods:   []string{http.MethodGet, http.MethodPatch, http.MethodHead, http.MethodPost, http.MethodPut, http.MethodDelete},
			AllowCredentials: true,
		})

		// Insert the middleware
		n.Use(cors)

		// Attach routes
		n.UseHandler(r)

		log.Printf("API version: `%s`", handler.GitRevision)
		n.Run(fmt.Sprintf(":%s", conf.Port))

	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	err := envconfig.Process("API", &conf)
	if err != nil {
		log.Fatal(err.Error())
	}
	c, err := json.Marshal(conf)
	if err != nil {
		log.Fatal(err.Error())
	}
	fmt.Printf("Configuration loaded %s", c)
}
