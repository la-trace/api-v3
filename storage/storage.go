package storage

import (
	"errors"

	geojson "github.com/paulmach/go.geojson"
)

var (
	ErrEmailAlreadyExists = errors.New("Email already exists")
	ErrNoRows             = errors.New("No Rows found")
)

type Storage interface {
	AddUser(AddingUser) (*User, error)

	// Named param is easier in implementation
	UpdatePassword(password, id string) error

	IncNumberOfDownloadsOfTrack(string) error

	GetTrackByID(TrackID string) (*Track, error)
	GetTracksByUserID(userID string, limit, offset string) ([]Track, string, error)
	GetTrackAsGeoJSONByIDAndFactor(string, float64) (*geojson.Geometry, error)
	GetPicturesByTrackID(string) ([]Picture, error)
	GetVideosByTrackID(string) ([]Video, error)
	GetCommentsByTrackID(string) ([]Comment, error)
	GetPoisByTrackID(string) ([]Poi, error)
	GetTrackIDNearLonLat(float64, float64) ([]NearTrack, error)
	GetTrackElevationByID(string) ([]Elevation, error)
	GetIntersectsTracksAsCentroidGeoJSON(swLon, swLat, neLon, neLat float64, q string) ([]TrackAsPoint, error)
	GetIntersectingTracksId(swLon, swLat, neLon, neLat float64, q string) ([]string, error)
	GetStarredTracks() ([]Starred, error)
	GetUserIDByEmail(email string) (string, error)
	GetMagicLinkByCode(code string) (*MagicLink, error)

	DeletePoi(string) error
	DeleteVideo(trackID string) error
	DeleteTrack(trackID string) error

	InsertVideo(platform, slug, trackID string) (*Video, error)
	InsertPicture(width, height int, lon, lat float64, id, fileExtension string) (*Picture, error)
	InsertPoi(caption, trackID string, lon float64, lat float64) (*Poi, error)
	InsertComment(content, trackID, userID string) error

	InsertMagicLink(code, userID, action string) error

	PatchTrack(string, EditableTrack) error
	PatchPoi(string, EditablePoi) error
	PatchPicture(string, EditablePicture) error
	PatchUser(string, EditableUser) error

	Login(email, password string) (*User, error)

	SetMagicLinkConsumed(code string) error
	SetEmailValidated(validationID, userID string) error

	LogDownload(userID, trackID string) error

	GetAllTrackID() ([]SiteMapTrack, error)
	GetHeatMap(swLon, swLat, neLon, neLat float64) (HeatMap, error)
	GetUserHeatMap(userID string) (HeatMap, error)

	GetTrackIDFromMySQLID(id string) (uuid string, err error)

	GetTiles(x, y, z int) ([]byte, error)
	CheckPostGisVersion() (bool, error)
}
