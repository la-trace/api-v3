package handler

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/la-trace/api-v3/api"
)

const (
	secret = "e25b41cb32477c34c52e8b4980393019d35495eb3267ed4c8aacba4000692974"
)

type loginPayLoad struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func forgeJWT(userID, secret string) (token string, err error) {
	claims := jwt.MapClaims{
		"Issuer":    "la-trace.com",
		"expiredAt": time.Now().Add(1440 * time.Minute).Unix(),
		"userId":    userID,
	}

	jwToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// sign and get the complete encoded token as a string using the secret
	token, err = jwToken.SignedString([]byte(secret))

	return token, err
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var p loginPayLoad
	err := decoder.Decode(&p)

	if err != nil {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	email := p.Email
	password := p.Password

	if email == "" || password == "" {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), errorBadParam.code)
		return
	}

	user, err := h.Dao.Login(email, password)

	if err != nil {
		if err == sql.ErrNoRows {
			api.JSONWithHTTPCode(w, errorBadCredential.payload(), errorBadCredential.code)
			return
		}
	}

	// New account (not imported from the previous mySQL Database)
	// must be validated before login
	if user.MySQLID == nil && user.ValidationID == nil {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), http.StatusForbidden)
		return
	}

	// if user.ID == "" {
	// 	api.JSONWithHTTPCode(w, errorBadCredential.payload(), errorBadCredential.code)
	// 	return
	// }

	jwtString, err := forgeJWT(user.ID, h.jwtKey)

	if err != nil {
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	}

	api.JSON(w, map[string]string{"token": jwtString})
}

func (h *Handler) Refresh(w http.ResponseWriter, r *http.Request) {
	j := GetJWTFromContext(r)

	if _, ok := j["userId"]; !ok {
		api.JSON(w, "Error no jwt")
		return
	}

	claims := jwt.MapClaims{
		"Issuer":    "la-trace.com",
		"expiredAt": time.Now().Add(1440 * time.Minute).Unix(),
		"userId":    j["userId"],
	}

	jwToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// sign and get the complete encoded token as a string using the secret
	jwtString, err := jwToken.SignedString([]byte(secret))

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
	}

	api.JSON(w, map[string]string{"token": jwtString})
}
