# build
FROM golang:1.14 as builder
WORKDIR /go/src/gitlab.com/la-trace/api-v3
ADD . /go/src/gitlab.com/la-trace/api-v3/
COPY config/ /srv/la-trace/config/
COPY assets/ /srv/la-trace/assets/
COPY migrations/postgres/ /srv/la-trace/migrations/
RUN go get
RUN export COMMIT_HASH=$(git log --format='%h' -n 1); \
export COMMIT_DATE=$(git log --format='%ad' --date=format:'%Y-%m-%d_%H:%M:%S' -n 1); \
CGO_ENABLED=0 \
GOOS=linux \
go build \
-ldflags "-X gitlab.com/la-trace/api-v3/handler.GitRevision=${COMMIT_HASH} \
-X gitlab.com/la-trace/api-v3/handler.GitDate=${COMMIT_DATE}" \
-o la-trace-api

# exec
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root
COPY --from=builder /go/src/gitlab.com/la-trace/api-v3/la-trace-api .
COPY --from=builder /srv/la-trace/ /srv/la-trace/
CMD ["./la-trace-api"]