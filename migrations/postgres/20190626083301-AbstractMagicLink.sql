
-- +migrate Up
CREATE TYPE magic_link_action AS ENUM ('none', 'email-login', 'email-verification');
ALTER TABLE magic_link ADD COLUMN action magic_link_action;
UPDATE magic_link SET action = 'email-login';

ALTER TABLE public.user ADD COLUMN validation_id VARCHAR;
ALTER TABLE public.user ADD CONSTRAINT user_fk_magic_link FOREIGN KEY (validation_id) REFERENCES "magic_link"(code);

-- +migrate Down
ALTER TABLE magic_link DROP COLUMN action;
ALTER TABLE public.user DROP COLUMN validation_id;
DROP TYPE magic_link_action;
