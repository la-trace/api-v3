
-- +migrate Up
-- +migrate StatementBegin

ALTER TABLE comment ALTER COLUMN creation SET DEFAULT now();

CREATE OR REPLACE FUNCTION trigger_set_timestamp() RETURNS TRIGGER AS $$
BEGIN
NEW.modification = NOW();
RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER set_timestamp_comment
BEFORE UPDATE ON comment
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
-- +migrate StatementEnd


-- +migrate Down
ALTER TABLE comment ALTER COLUMN creation DROP DEFAULT;
DROP TRIGGER set_timestamp_comment ON comment;
DROP FUNCTION trigger_set_timestamp();

