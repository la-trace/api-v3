package cmd

import (
	"testing"
)

func Test_replaceWithStars(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Main",
			args: args{
				s: "S2Pjf72V7xp3PPEj",
			},
			want: "************PEj",
		},
		{
			name: "Less than minimal size",
			args: args{
				s: "Ej",
			},
			want: "Ej",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := replaceWithStars(tt.args.s); got != tt.want {
				t.Errorf("replaceWithStars() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_hideCredentialsInConnectionString(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Main",
			args: args{
				s: "postgres://foo:bar@localhost/la-trace?sslmode=disable",
			},
			want: "postgres://*****:*****@localhost/la-trace?sslmode=disable",
		},
		{
			name: "Main",
			args: args{
				s: "postgres://foo:){}}@localhost/la-trace?sslmode=disable",
			},
			want: "postgres://*****:*****@localhost/la-trace?sslmode=disable",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := hideCredentialsInConnectionString(tt.args.s); got != tt.want {
				t.Errorf("hideCredentialsInConnectionString() = %v, want %v", got, tt.want)
			}
		})
	}
}
