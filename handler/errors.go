package handler

type webError struct {
	message string
	code    int
}

type webErrorPayload struct {
	Message string `json:"message"`
}

func (e webError) String() string {
	return e.message
}

func (e webError) payload() webErrorPayload {
	return webErrorPayload{Message: e.message}
}

func newError(msg string, code int) webError {
	return webError{message: msg, code: code}
}

var errorNotFound = newError("Sorry, we can't found the entity you are looking for", 404)
var errorDatabase = newError("Sorry, an error occurs with the database", 500)
var errorBadParam = newError("At least one paramater is incorrect", 400)
var errorBadCredential = newError("Bad Authentication", 401)
