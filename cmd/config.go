package cmd

import (
	"encoding/json"
	"regexp"
)

var conf config

type config struct {
	PG                 string
	EmailTemplates     string
	Migrations         string
	Pictures           string
	Queries            string
	Port               string
	FrontHost          string
	StravaClientID     string
	StravaClientSecret string
	StravaRedirectURL  string
	SendGridKey        string
	PasswordSalt       string
	JWTkey             string
	RenderGeoJSONURL   string
}

func hideCredentialsInConnectionString(s string) string {
	re := regexp.MustCompile(`\w+:[A-Za-z0-9-(){}]+`)
	return re.ReplaceAllString(s, "*****:*****")
}

func replaceWithStars(s string) string {
	unmaskedSize := 3
	if len(s) <= unmaskedSize {
		return s
	}
	buf := ""
	prefix := s[:len(s)-unmaskedSize]
	for i := 1; i < len(prefix); i++ {
		buf += "*"
	}
	return buf + s[len(s)-unmaskedSize:]
}

func (c config) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		PG                 string
		EmailTemplates     string
		Migrations         string
		Pictures           string
		Queries            string
		Port               string
		FrontHost          string
		StravaClientID     string
		StravaClientSecret string
		StravaRedirectURL  string
		SendGridKey        string
		PasswordSalt       string
		JWTkey             string
		RenderGeoJSONURL   string
	}{
		PG:                 hideCredentialsInConnectionString(c.PG),
		EmailTemplates:     c.EmailTemplates,
		Migrations:         c.Migrations,
		Pictures:           c.Pictures,
		Queries:            c.Queries,
		Port:               c.Port,
		FrontHost:          c.FrontHost,
		StravaClientID:     c.StravaClientID,
		StravaClientSecret: replaceWithStars(c.StravaClientSecret),
		StravaRedirectURL:  c.StravaRedirectURL,
		SendGridKey:        replaceWithStars(c.SendGridKey),
		PasswordSalt:       replaceWithStars(c.PasswordSalt),
		JWTkey:             replaceWithStars(c.JWTkey),
		RenderGeoJSONURL:   c.RenderGeoJSONURL,
	})
}
