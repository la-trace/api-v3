package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/email"
	"gitlab.com/la-trace/api-v3/storage"
	validator "gopkg.in/go-playground/validator.v9"
)

func (h *Handler) Contact(w http.ResponseWriter, r *http.Request) {

	c := storage.Contact{}
	err := api.GetJSONContent(&c, r)

	err = h.Validator.Struct(&c)
	fields := map[string]string{}

	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			fields[err.Field()] = err.ActualTag()
		}
		api.UnprocessableError(w, fields)
		return
	}

	m := email.Message{}
	m.HTMLContent = fmt.Sprintf("%s\n%s", c.Email, c.Content)
	m.TextContent = fmt.Sprintf("%s\n%s", c.Email, c.Content)
	sender := email.NewContact("la-trace.com", "hello@la-trace.com")
	recipient := email.NewContact("la-trace.com", "hello@la-trace.com")

	m.Recipient = recipient
	m.Sender = sender
	m.Subject = "Contact Form"

	err = h.EmailClient.Send(m)

	if err != nil {
		api.Error(w, "Error when sending email", http.StatusInternalServerError)
		return
	}

	api.JSONWithHTTPCode(w, nil, http.StatusNoContent)
}
