package handler

// ErrUnsupportedDataType  is returned by Scan methods when asked to scan
import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type Key int

const (
	authRequestKey     = "Authorization"
	tokenType          = "Bearer"
	jwtContextKey  Key = iota
)

// ErrUnsupportedDataType non []byte data from the database.
var ErrUnsupportedDataType = errors.New("scan value must be []byte")

// JWTToContext returns a negoni HandlerFunc to be uses as a middleware
// It looks for a JWT (in Authorization header)
// If the JWT is valid, pass it into the context
func CheckAuth(secretKey string, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// token validation
		authorization := r.Header.Get(authRequestKey)
		tokenString := ""
		// parse authorization
		if authorization != "" {
			el := strings.Split(authorization, " ")
			if len(el) == 2 && el[0] == tokenType {
				tokenString = el[1]
			} else {
				http.Error(w, "JWT malformed", http.StatusForbidden)
				return
			}
		}

		if tokenString == "" {
			http.Error(w, "JWT is not given", http.StatusForbidden)
			return
		}

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(secretKey), nil
		})

		if err != nil {
			http.Error(w, "JWT is invalid", http.StatusUnauthorized)
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			expiredAtAsFloat := claims["expiredAt"].(float64)
			expiredAt := time.Unix(int64(expiredAtAsFloat), 0)
			now := time.Now()
			diff := expiredAt.Sub(now)

			if diff < 0 {
				http.Error(w, "JWT is expired", http.StatusUnauthorized)
				return
			}

			ctx := context.WithValue(r.Context(), jwtContextKey, claims)
			next(w, r.WithContext(ctx))
			return
		}

		http.Error(w, "JWT is invalid", http.StatusUnauthorized)
		return

	}
}

// GetJWTFromContext returns the JWT from the request context
func GetJWTFromContext(r *http.Request) jwt.MapClaims {
	v := r.Context().Value(jwtContextKey)
	if v == nil {
		return nil
	}
	jwt, ok := v.(jwt.MapClaims)
	if ok {
		return jwt
	}
	return nil
}
