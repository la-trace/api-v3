package storage

import (
	"encoding/json"
	"strconv"
	"time"

	geojson "github.com/paulmach/go.geojson"
)

// NearTrack entity
// @openapi:schema
type NearTrack struct {
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Distance    float64 `json:"distamce"`
	ID          string  `json:"id"`
}

// Elevation entity
// @openapi:schema
type Elevation []float64

// TrackAsPoint entity
type TrackAsPoint struct {
	ID    string           `json:"id"`
	Point geojson.Geometry `json:"point"`
}

// User entity
// @openapi:schema
type User struct {
	ID           string     `json:"id"`
	Creation     time.Time  `json:"creation"`
	Modification *time.Time `json:"modification"`
	Name         string     `json:"name"`
	MySQLID      *int       `json:"-"`
	ValidationID *string    `json:"-"`
}

// Address entity
// @openapi:schema
type Address struct {
	City        string            `json:"city"`
	CountryCode string            `json:"countryCode"`
	Position    *geojson.Geometry `json:"position"`
	Longitude   float64           `json:"longitude"`
	Latitude    float64           `json:"latitude"`
}

func NewAddress() Address {
	a := Address{}
	a.Position = &geojson.Geometry{}
	return a
}

// FromJSONString from PostGIS
func (a *Address) FromJSONString(b []byte) error {
	if len(b) == 0 {
		return nil
	}

	buff := OsmPlace{}
	err := json.Unmarshal(b, &buff)
	if err != nil {
		return err
	}
	a.CountryCode = buff.Address.CountryCode

	if buff.Address.City != "" {
		a.City = buff.Address.City
	}

	if buff.Address.Town != "" {
		a.City = buff.Address.Town
	}

	if buff.Address.Village != "" {
		a.City = buff.Address.Village
	}

	if buff.Address.Hamlet != "" {
		a.City = buff.Address.Hamlet
	}

	lat, err := strconv.ParseFloat(buff.Lat, 64)

	if err == nil {
		a.Latitude = lat
	}

	lon, err := strconv.ParseFloat(buff.Lon, 64)

	if err == nil {
		a.Longitude = lon
	}

	a.Position = geojson.NewPointGeometry([]float64{lon, lat})

	return nil
}

// Track entity
// @openapi:schema
type Track struct {
	ID            string    `json:"id"`
	Name          string    `json:"name"`
	Description   string    `json:"description"`
	Distance      int       `json:"distance"`
	ElevationGain int       `json:"elevationGain"`
	ElevationLoss int       `json:"elevationLoss"`
	Creation      time.Time `json:"creation"`
	User          User      `json:"user"`
	Pictures      []Picture `json:"pictures"`
	Videos        []Video   `json:"video"`
	Pois          []Poi     `json:"pois"`
	Comments      []Comment `json:"comments"`
	StartAddress  *Address  `json:"startAddress"`
	EndAddress    *Address  `json:"endAddress"`
	Rating        Rating    `json:"rating"`
	Downloads     int       `json:"downloads"`
}

// Rating entity
// @openapi:schema
type Rating map[string]interface{}

// Comment entity
// @openapi:schema
type Comment struct {
	ID           string     `json:"id"`
	Creation     time.Time  `json:"creation"`
	Modification *time.Time `json:"modification"`
	User         User       `json:"user"`
	Content      string     `json:"content"`
}

// OsmPlace entity
// @openapi:schema
type OsmPlace struct {
	DisplayName string      `json:"display_name"`
	PlaceID     interface{} `json:"place_id"`
	Lon         string      `json:"lon"`
	Boundingbox []string    `json:"boundingbox"`
	OsmType     string      `json:"osm_type"`
	Licence     string      `json:"licence"`
	OsmID       interface{} `json:"osm_id"`
	Lat         string      `json:"lat"`
	Address     OSMAddress  `json:"address"`
}

// Video entity
// @openapi:schema
type Video struct {
	ID           *string    `json:"id"`
	Creation     *time.Time `json:"creation"`
	Modification *time.Time `json:"modification"`
	Platform     *string    `json:"platform"`
	Slug         *string    `json:"slug"`
	TrackID      *string    `json:"trackId"`
}

// OSMAddress entoty
// @openapi:schema
type OSMAddress struct {
	Town           string `json:"town"`
	PublicBuilding string `json:"public_building"`
	Country        string `json:"country"`
	County         string `json:"county"`
	State          string `json:"state"`
	Postcode       string `json:"postcode"`
	CountryCode    string `json:"country_code"`
	Road           string `json:"road"`
	Village        string `json:"village"`
	Hamlet         string `json:"hamlet"`
	City           string `json:"city"`
}

// Poi entity
// @openapi:schema
type Poi struct {
	ID           string            `json:"id"`
	Creation     *time.Time        `json:"creation"`
	Modification *time.Time        `json:"modification"`
	Icon         string            `json:"icon"`
	Position     *geojson.Geometry `json:"position"`
	Caption      string            `json:"caption"`
}

// NewPoi poi constructor
func NewPoi() Poi {
	p := Poi{}
	p.Position = &geojson.Geometry{}
	return p
}

// Picture entity
// @openapi:schema
type Picture struct {
	ID       string            `json:"id"`
	Creation time.Time         `json:"creation"`
	Caption  *string           `json:"caption"`
	Width    int               `json:"width"`
	Height   int               `json:"height"`
	URL      string            `json:"url"`
	File     string            `json:"-"`
	Position *geojson.Geometry `json:"position"`
}

func NewPicture() Picture {
	p := Picture{}
	p.Position = &geojson.Geometry{}
	return p
}

// @openapi:schema:geojson
// hack for openapi documentation, not used directly by the API
type geometry struct {
	Type        string                 `json:"type"`
	BoundingBox []float64              `json:"bbox,omitempty"`
	Coordinates interface{}            `json:"coordinates,omitempty"`
	Geometries  interface{}            `json:"geometries,omitempty"`
	CRS         map[string]interface{} `json:"crs,omitempty"`
}

type Contact struct {
	Email   string `validate:"required,email" json:"email"`
	Content string `validate:"required" json:"content"`
}

type Starred struct {
	ID            string   `json:"id"`
	Name          string   `json:"name"`
	Distance      int      `json:"distance"`
	ElevationGain int      `json:"elevationGain"`
	Picture       string   `json:"picture"`
	StartAddress  *Address `json:"startAddress"`
}

type UserLight struct {
	ID       string     `json:"id"`
	Name     string     `validate:"required" json:"name"`
	Email    string     `validate:"required,email" json:"email"`
	Creation *time.Time `json:"creation,omitempty"`
}

type MagicLink struct {
	Code       string     `json:"code"`
	CreatedAt  time.Time  `json:"createdAt"`
	ConsumedAt *time.Time `json:"consumed"`
	UserID     string     `json:"userID"`
	Action     string     `json:"action"`
}

type MagicLinkRequest struct {
	Code string `json:"code"`
}

type SiteMapTrack struct {
	ID           string     `json:"id"`
	Modification *time.Time `json:"creation,omitempty"`
}

type HeatMapPoint [3]float64
type HeatMap []HeatMapPoint
