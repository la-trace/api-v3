package handler

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/storage"
)

func (h *Handler) Redirect(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["name"]
	id := vars["id"]

	if id == "" || name == "" {
		api.Error(w, "BadParam", http.StatusBadRequest)
	}

	uuid, err := h.Dao.GetTrackIDFromMySQLID(id)

	switch {
	case err == storage.ErrNoRows:
		api.Error(w, "Could not find track", http.StatusNotFound)
		return
	case err != nil:
		log.Println(err)
		api.Error(w, "Error trying to redirect", http.StatusInternalServerError)
		return
	}

	url := h.FrontHost + "/fr/track/" + uuid
	http.Redirect(w, r, url, http.StatusMovedPermanently)

}
