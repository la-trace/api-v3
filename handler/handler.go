package handler

import (
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http/httputil"
	"net/url"
	"reflect"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	//Insert pg
	_ "github.com/lib/pq"
	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/la-trace/api-v3/email"
	"gitlab.com/la-trace/api-v3/storage"
	validator "gopkg.in/go-playground/validator.v9"
)

var GitRevision string
var GitDate string

type templateName string
type locale string
type templateByLocale map[locale]*template.Template
type templateList map[templateName]templateByLocale

type Handler struct {
	DB           *sql.DB
	Paths        map[string]string
	Dao          storage.Storage
	EmailClient  email.Provider
	FrontHost    string
	Validator    *validator.Validate
	templateList templateList
	jwtKey       string
	renderProxy  *httputil.ReverseProxy
}

func (h *Handler) PlayMigration(path string) {
	migrations := &migrate.FileMigrationSource{
		Dir: path,
	}
	migrate.SetTable("migrations")
	n, err := migrate.Exec(h.DB, "postgres", migrations, migrate.Up)
	if err != nil {
		logrus.Fatal(err)
	}
	logrus.Infof("Applied %d migrations", n)
}

func NewHandler(pg, pix, sendGridKey, host string, dao storage.Storage, jwtKey, renderGeoJSONProxy string) (*Handler, error) {
	db, err := sql.Open("postgres", pg)

	v := validator.New()
	v.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	url, err := url.Parse(renderGeoJSONProxy)
	if err != nil {
		return nil, err
	}

	p := httputil.NewSingleHostReverseProxy(url)

	var client = email.New(sendGridKey)

	return &Handler{
		Dao:         dao,
		DB:          db,
		EmailClient: client,
		FrontHost:   host,
		Paths: map[string]string{
			"pictures": pix,
		},
		renderProxy:  p,
		Validator:    v,
		templateList: templateList{},
		jwtKey:       jwtKey,
	}, err
}

func (h *Handler) RegisterTemplate(n templateName, l locale, path string) error {
	dat, err := ioutil.ReadFile(path)

	if err != nil {
		return err
	}

	tmpl, err := template.New(string(n)).Parse(string(dat))

	// the name doesn't exist yet
	if _, ok := h.templateList[n]; !ok {
		h.templateList[n] = templateByLocale{
			l: tmpl,
		}
		return nil
	}

	// locale doesn't exist
	tplbyLocale := h.templateList[n]
	if _, ok := tplbyLocale[l]; !ok {
		h.templateList[n][l] = tmpl
		return nil
	}

	// Name and locale already exists
	// TODO: Returns an error 🤔
	h.templateList[n][l] = tmpl

	return nil
}

func (h *Handler) GetTemplate(n templateName, l locale) (*template.Template, error) {
	// the name doesn't exist yet
	if _, ok := h.templateList[n]; !ok {
		return nil, errors.New("Template does not exist")
	}

	// Test if locale exists
	tplbyLocale := h.templateList[n]
	if _, ok := tplbyLocale[l]; !ok {
		return nil, fmt.Errorf("Locale does not exist for this template %s", n)
	}

	return h.templateList[n][l], nil
}
