package mock

import (
	geojson "github.com/paulmach/go.geojson"
	"gitlab.com/la-trace/api-v3/storage"
)

type Mock struct {
}

func (m *Mock) AddUser(storage.AddingUser) (*storage.User, error) {
	return nil, nil
}

func (m *Mock) UpdatePassword(password string, id string) error {
	return nil
}

func (m *Mock) IncNumberOfDownloadsOfTrack(string) error {
	return nil
}

func (m *Mock) GetTrackByID(TrackID string) (*storage.Track, error) {
	return nil, nil
}

func (m *Mock) GetTrackAsGeoJSONByIDAndFactor(string, float64) (*geojson.Geometry, error) {
	return nil, nil
}

func (m *Mock) GetPicturesByTrackID(string) ([]storage.Picture, error) {
	return nil, nil
}

func (m *Mock) GetVideosByTrackID(string) ([]storage.Video, error) {
	return nil, nil
}

func (m *Mock) GetCommentsByTrackID(string) ([]storage.Comment, error) {
	return nil, nil
}

func (m *Mock) GetPoisByTrackID(string) ([]storage.Poi, error) {
	return nil, nil
}

func (m *Mock) GetTrackIDNearLonLat(float64, float64) ([]storage.NearTrack, error) {
	return nil, nil
}

func (m *Mock) GetTrackElevationByID(string) ([]storage.Elevation, error) {
	return nil, nil
}

func (m *Mock) GetIntersectsTracksAsCentroidGeoJSON(swLon float64, swLat float64, neLon float64, neLat float64, q string) ([]storage.TrackAsPoint, error) {
	return nil, nil
}

func (m *Mock) GetIntersectingTracksId(swLon float64, swLat float64, neLon float64, neLat float64, q string) ([]string, error) {
	return nil, nil
}

func (m *Mock) GetStarredTracks() ([]storage.Starred, error) {
	return nil, nil
}

func (m *Mock) GetUserIDByEmail(email string) (string, error) {
	return "", nil
}

func (m *Mock) GetMagicLinkByCode(code string) (*storage.MagicLink, error) {
	return nil, nil
}

func (m *Mock) DeletePoi(string) error {
	return nil
}

func (m *Mock) DeleteVideo(trackID string) error {
	return nil
}

func (m *Mock) DeleteTrack(trackID string) error {
	return nil
}

func (m *Mock) InsertVideo(platform string, slug string, trackID string) (*storage.Video, error) {
	return nil, nil
}

func (m *Mock) InsertPicture(width int, height int, lon float64, lat float64, id string, fileExtension string) (*storage.Picture, error) {
	return nil, nil
}

func (m *Mock) InsertPoi(caption string, trackID string, lon float64, lat float64) (*storage.Poi, error) {
	return nil, nil
}

func (m *Mock) InsertComment(content string, trackID string, userID string) error {
	return nil
}

func (m *Mock) InsertMagicLink(code string, userID string, action string) error {
	return nil
}

func (m *Mock) PatchTrack(string, storage.EditableTrack) error {
	return nil
}

func (m *Mock) PatchPoi(string, storage.EditablePoi) error {
	return nil
}

func (m *Mock) PatchPicture(string, storage.EditablePicture) error {
	return nil
}

func (m *Mock) PatchUser(string, storage.EditableUser) error {
	return nil
}

func (m *Mock) Login(email string, password string) (*storage.User, error) {
	return nil, nil
}

func (m *Mock) SetMagicLinkConsumed(code string) error {
	return nil
}

func (m *Mock) SetEmailValidated(validationID string, userID string) error {
	return nil
}

func (m *Mock) GetAllTrackID() ([]storage.SiteMapTrack, error) {
	return nil, nil
}

func (m *Mock) GetHeatMap(swLon, swLat, neLon, neLat float64) (storage.HeatMap, error) {
	return nil, nil
}

func (m *Mock) GetTrackIDFromMySQLID(id string) (uuid string, err error) {
	return "8229f9ff-ac00-4ad2-84fc-3cf7d73ad0eb", nil
}

func (m *Mock) LogDownload(userID, trackID string) error {
	return nil
}

func (m *Mock) GetTiles(x, y, z int) ([]byte, error) {
	return nil, nil
}

func (p *Mock) CheckPostGisVersion() (bool, error) {
	return true, nil
}

func (p *Mock) GetUserHeatMap(userID string) (storage.HeatMap, error) {
	return nil, nil
}

func (p *Mock) GetTracksByUserID(userID string, limit, offset string) ([]storage.Track, string, error) {
	return nil, "", nil
}
