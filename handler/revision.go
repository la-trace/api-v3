package handler

import (
	"net/http"

	"gitlab.com/la-trace/api-v3/api"
)

func (h *Handler) Revision(w http.ResponseWriter, r *http.Request) {
	api.JSON(w, map[string]string{
		"revision": GitRevision,
		"date":     GitDate,
	})
}
