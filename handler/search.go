package handler

import (
	"log"
	"net/http"

	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/storage"
)

func (h *Handler) Search(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query().Get("q")
	limit := r.URL.Query().Get("limit")

	if limit == "" {
		limit = "20"
	}

	offset := r.URL.Query().Get("offset")

	if offset == "" {
		offset = "0"
	}

	start := []byte{}
	end := []byte{}

	rows, err := h.DB.Query(`
		SELECT
			t.id, t.name, t.description, t.elevation_gain, t.elevation_loss, t.creation, t.distance, t.start_point, t.end_point,
			u.id, u.name, u.creation, u.modification,
			count(*) OVER() AS full_count
		FROM
			public.track t
		LEFT JOIN
			"user" u
		ON
			u.id = t.user_id
		WHERE
			textsearchable @@ plainto_tsquery('french', $1)
		ORDER BY
			ts_rank(textsearchable, plainto_tsquery('french', $1)) DESC
		LIMIT $2 OFFSET $3`, q, limit, offset)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	}

	tracks := []storage.Track{}
	count := ""

	for rows.Next() {
		t := storage.Track{}
		rows.Scan(&t.ID, &t.Name, &t.Description, &t.ElevationGain, &t.ElevationLoss, &t.Creation, &t.Distance, &start, &end, &t.User.ID, &t.User.Name, &t.User.Creation, &t.User.Modification, &count)

		t.StartAddress = &storage.Address{}
		t.EndAddress = &storage.Address{}
		err = t.StartAddress.FromJSONString(start)
		if err != nil {
			log.Printf("Database error: %s", err)
			api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
			return
		}

		t.EndAddress.FromJSONString(end)
		if err != nil {
			log.Printf("Database error: %s", err)
			api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
			return
		}

		tracks = append(tracks, t)
	}

	w.Header().Set("X-Total-Count", count)
	w.Header().Set("Access-Control-Expose-Headers", "X-Total-Count")

	api.JSON(w, tracks)
}

func (h *Handler) Last(w http.ResponseWriter, r *http.Request) {

	rows, err := h.DB.Query(`
		SELECT
			t.id, t.name, t.description, t.elevation_gain, t.elevation_loss, t.creation, t.distance,
			u.id, u.name, u.creation, u.modification
		FROM
			public.track t
		LEFT JOIN
			"user" u
		ON
			u.id = t.user_id
		ORDER BY
			t.creation DESC
		LIMIT 10
			`)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	}

	tracks := []storage.Track{}

	for rows.Next() {
		t := storage.Track{}
		rows.Scan(&t.ID, &t.Name, &t.Description, &t.ElevationGain, &t.ElevationLoss, &t.Creation, &t.Distance, &t.User.ID, &t.User.Name, &t.User.Creation, &t.User.Modification)
		tracks = append(tracks, t)
	}

	api.JSON(w, tracks)
}
