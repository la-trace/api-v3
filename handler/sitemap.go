package handler

import (
	"fmt"
	"net/http"

	"github.com/snabb/sitemap"
)

const lg = "fr"

func (h *Handler) generateUrl(endpoint string) string {
	return h.FrontHost + "/" + lg + endpoint
}

func (h *Handler) generateTrackUrl(trackID string) string {
	return h.FrontHost + "/" + lg + "/track/" + trackID
}

var staticRoutes = []string{
	"/",
	"/search",
	"/login",
	"/register",
	"/connector/strava",
}

func (h *Handler) Sitemap(w http.ResponseWriter, r *http.Request) {
	sm := sitemap.New()
	sm.Minify = true
	tracks, err := h.Dao.GetAllTrackID()

	if err != nil {
		fmt.Println(err)
	}

	for _, r := range staticRoutes {
		sm.Add(&sitemap.URL{
			Loc:        h.generateUrl(r),
			LastMod:    nil,
			ChangeFreq: sitemap.Daily,
		})
	}
	for _, t := range tracks {
		sm.Add(&sitemap.URL{
			Loc:        h.generateTrackUrl(t.ID),
			LastMod:    nil,
			ChangeFreq: sitemap.Monthly,
		})
	}
	sm.WriteTo(w)
}
