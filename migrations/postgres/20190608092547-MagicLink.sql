-- +migrate Up
CREATE TABLE magic_link (
    code character(20),
    created_at timestamp with time zone DEFAULT now(),
    consumed_at timestamp with time zone,
    creation_data json,
    consumption_data json,
    user_id uuid
);

ALTER TABLE magic_link ADD CONSTRAINT magic_link_pkey PRIMARY KEY (code);
ALTER TABLE magic_link ADD CONSTRAINT magic_link_fk_user FOREIGN KEY (user_id) REFERENCES "user"(id);

-- +migrate Down
DROP TABLE magic_link;