package storage

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strings"

	geojson "github.com/paulmach/go.geojson"
)

type cursor struct {
	value int
}

func newCursor() *cursor {
	return &cursor{}
}

func (c *cursor) use() int {
	c.value++
	return c.value
}

func (c *cursor) get() int {
	return c.value
}

type PatchSet struct {
	Fields       string
	Placeholders string
	Values       []interface{}
	Cursor       int
}

// inspired from sql.NullString https://golang.org/pkg/database/sql/#NullString
type NullableString struct {
	String string
	Set    bool
	Valid  bool
}

func (n *NullableString) UnmarshalJSON(data []byte) error {
	n.Set = true

	if string(data) == "null" {
		// The key was set to null
		n.Valid = false
		return nil
	}

	var buffer string
	if err := json.Unmarshal(data, &buffer); err != nil {
		return err
	}
	n.String = buffer
	n.Valid = true
	return nil
}

func (n NullableString) Value() (driver.Value, error) {
	if !n.Valid {
		return nil, nil
	}
	return n.String, nil
}

type EditablePoi struct {
	Icon     NullableString   `json:"icon,omitempty"`
	Position NullablePosition `json:"position,omitempty"`
	Caption  NullableString   `json:"caption,omitempty"`
}

func (ep EditablePoi) Patch() PatchSet {
	c := newCursor()

	fields := []string{}
	placeholders := []string{}
	values := []interface{}{}

	if ep.Icon.Set {
		fields = append(fields, "icon")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		values = append(values, ep.Icon)
	}

	if ep.Position.Set {
		fields = append(fields, "position")
		placeholders = append(placeholders, fmt.Sprintf("ST_Point($%d, $%d)", c.use(), c.use()))
		values = append(values, RoundPlus(ep.Position.Value.Point[0], 5), RoundPlus(ep.Position.Value.Point[1], 5))
	}

	if ep.Caption.Set {
		fields = append(fields, "caption")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		values = append(values, ep.Caption)
	}

	return PatchSet{
		Fields:       strings.Join(fields, ", "),
		Placeholders: strings.Join(placeholders, ", "),
		Values:       values,
		Cursor:       c.use(),
	}
}

type NullablePosition struct {
	Value *geojson.Geometry
	Set   bool
	Valid bool
}

func (n *NullablePosition) UnmarshalJSON(data []byte) error {
	n.Set = true

	if string(data) == "null" {
		// The key was set to null
		n.Valid = false
		return nil
	}

	var p geojson.Geometry
	if err := json.Unmarshal(data, &p); err != nil {
		return err
	}
	n.Value = &p
	n.Valid = true
	return nil
}

type NullableRating struct {
	Value *Rating
	Set   bool
	Valid bool
}

func (n *NullableRating) UnmarshalJSON(data []byte) error {
	n.Set = true

	if string(data) == "null" {
		// The key was set to null
		n.Valid = false
		return nil
	}

	var p Rating
	if err := json.Unmarshal(data, &p); err != nil {
		return err
	}
	n.Value = &p
	n.Valid = true
	return nil
}

type EditablePicture struct {
	Caption  NullableString   `json:"caption,omitempty"`
	Position NullablePosition `json:"position,omitempty"`
}

func (ep EditablePicture) Patch() PatchSet {
	c := newCursor()

	fields := []string{}
	placeholders := []string{}
	values := []interface{}{}

	if ep.Position.Set {
		fields = append(fields, "position")
		placeholders = append(placeholders, fmt.Sprintf("ST_Point($%d, $%d)", c.use(), c.use()))
		values = append(values, RoundPlus(ep.Position.Value.Point[0], 5), RoundPlus(ep.Position.Value.Point[1], 5))
	}

	if ep.Caption.Set {
		fields = append(fields, "caption")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		values = append(values, ep.Caption)
	}

	return PatchSet{
		Fields:       strings.Join(fields, ", "),
		Placeholders: strings.Join(placeholders, ", "),
		Values:       values,
		Cursor:       c.use(),
	}
}

type EditableTrack struct {
	Name        NullableString `json:"name,omitempty" sql:"name"`
	Description NullableString `json:"description,omitempty" sql:"description"`
	Rating      NullableRating `json:"rating,omitempty" sql:"rating"`
}

func (et EditableTrack) Patch() PatchSet {
	c := newCursor()

	fields := []string{}
	placeholders := []string{}
	values := []interface{}{}

	if et.Name.Set {
		fields = append(fields, "name")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		values = append(values, et.Name)
	}

	if et.Description.Set {
		fields = append(fields, "description")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		values = append(values, et.Description)
	}

	if et.Rating.Set {
		fields = append(fields, "rating")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		content, err := json.Marshal(et.Rating.Value)
		if err != nil {
			fmt.Println(err)
		}
		values = append(values, content)

	}

	return PatchSet{
		Fields:       strings.Join(fields, ", "),
		Placeholders: strings.Join(placeholders, ", "),
		Values:       values,
		Cursor:       c.use(),
	}
}

type AddingUser struct {
	Name     string `validate:"required" json:"name"`
	Email    string `validate:"required,email" json:"email"`
	Password string `validate:"required" json:"password"`
}

type EditableUser struct {
	Name     NullableString `validate:"required" json:"name"`
	Email    NullableString `validate:"required,email" json:"email"`
	Password NullableString `validate:"required" json:"password"`
}

func (eu EditableUser) Patch() PatchSet {
	c := newCursor()

	fields := []string{}
	placeholders := []string{}
	values := []interface{}{}

	if eu.Name.Set {
		fields = append(fields, "name")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		values = append(values, eu.Name)
	}

	if eu.Email.Set {
		fields = append(fields, "email")
		placeholders = append(placeholders, fmt.Sprintf("$%d", c.use()))
		values = append(values, eu.Email)
	}

	return PatchSet{
		Fields:       strings.Join(fields, ", "),
		Placeholders: strings.Join(placeholders, ", "),
		Values:       values,
		Cursor:       c.use(),
	}
}

type EditablePassword struct {
	Password string `json:"password"`
}
