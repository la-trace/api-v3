-- +migrate Up
-- +migrate StatementBegin
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;
COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';
--
-- Name: videoplatform; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE videoplatform AS ENUM (
    '',
    'youtube',
    'vimeo',
    'dailymotion'
);


ALTER TYPE videoplatform OWNER TO postgres;

-- Name: track; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE track (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    file text,
    elevation_gain integer,
    elevation_loss integer,
    user_mysql integer,
    id_mysql integer,
    file_hash character(32),
    creation timestamp without time zone DEFAULT now(),
    distance integer,
    rating json,
    activity character varying,
    user_id uuid,
    start_point json,
    end_point json,
    textsearchable tsvector,
    linestring geography,
    downloads integer
);


ALTER TABLE track OWNER TO postgres;


--
-- Name: sp_calculate_distance(); Type: FUNCTION; Schema: public; Owner: postgres
--
CREATE FUNCTION sp_calculate_distance() RETURNS trigger
    LANGUAGE plpgsql
    AS $BODY$
BEGIN
	NEW.distance = ROUND(ST_Length(NEW.linestring));
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.sp_calculate_distance() OWNER TO postgres;

CREATE OR REPLACE FUNCTION sp_calculate_search_vector() RETURNS TRIGGER AS $$
BEGIN
NEW.textsearchable = setweight(to_tsvector('french', coalesce(NEW.name,'')), 'A') ||
setweight(to_tsvector('french', coalesce(NEW.description,'')), 'B') ||
setweight(to_tsvector('french', coalesce(NEW.start_point->>'display_name','')), 'B');
return NEW;
END;
$$ language 'plpgsql' ;

CREATE TRIGGER trigger_calculate_ts BEFORE INSERT OR UPDATE ON track FOR EACH ROW EXECUTE PROCEDURE sp_calculate_search_vector();
-- CREATE TRIGGER trigger_calculate_ts BEFORE INSERT ON track FOR EACH ROW EXECUTE PROCEDURE sp_calculate_distance();

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE comment (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    track_id uuid,
    content text,
    mysql_track_id integer,
    user_id uuid,
    mysql_id integer,
    mysql_user_id integer,
    creation timestamp with time zone,
    modification timestamp with time zone
);


ALTER TABLE comment OWNER TO postgres;

--
-- Name: picture; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE picture (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    caption text,
    width integer,
    height integer,
    track_id uuid,
    mysql_track_id integer,
    creation timestamp with time zone DEFAULT now(),
    modification timestamp with time zone,
    file text,
    "position" geography,
    mysql_id integer,
    file_extension text
);


ALTER TABLE picture OWNER TO postgres;

--
-- Name: poi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE poi (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    caption text,
    "position" geography,
    track_id uuid,
    mysql_id integer,
    mysql_track_id integer,
    mysql_icon_id text,
    creation timestamp with time zone DEFAULT now(),
    modification timestamp with time zone,
    icon text
);


ALTER TABLE poi OWNER TO postgres;

--


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    name character varying,
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    password text,
    id_mysql integer,
    email text,
    creation timestamp with time zone DEFAULT now(),
    modification timestamp with time zone
);


ALTER TABLE "user" OWNER TO postgres;

--
-- Name: video; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE video (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    creation timestamp with time zone DEFAULT now(),
    modification timestamp with time zone,
    platform videoplatform,
    slug text,
    track_id uuid,
    mysql_id integer
);


ALTER TABLE video OWNER TO postgres;

--
-- Name: comment comment_mysql_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comment_mysql_id_key UNIQUE (mysql_id);


--
-- Name: comment comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comment
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: user id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- Name: picture picture_mysql_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY picture
    ADD CONSTRAINT picture_mysql_id_key UNIQUE (mysql_id);


--
-- Name: picture picture_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY picture
    ADD CONSTRAINT picture_pkey PRIMARY KEY (id);


--
-- Name: poi poi_mysql_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poi
    ADD CONSTRAINT poi_mysql_id_key UNIQUE (mysql_id);


--
-- Name: poi poi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poi
    ADD CONSTRAINT poi_pkey PRIMARY KEY (id);


--
-- Name: track track_id_mysql_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY track
    ADD CONSTRAINT track_id_mysql_key UNIQUE (id_mysql);


--
-- Name: track track_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY track
    ADD CONSTRAINT track_pkey PRIMARY KEY (id);


--
-- Name: user user_mysql_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_mysql_id_key UNIQUE (id_mysql);


--
-- Name: video video_mysql_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY video
    ADD CONSTRAINT video_mysql_id_key UNIQUE (mysql_id);


--
-- Name: video video_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY video
    ADD CONSTRAINT video_pkey PRIMARY KEY (id);


--
-- Name: linestring_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX linestring_idx ON track USING gist (linestring);


--
-- Name: textsearch_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX textsearch_idx ON track USING gin (textsearchable);


--
-- Name: track trigger_calculate_distance; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_calculate_distance BEFORE INSERT ON track FOR EACH ROW EXECUTE PROCEDURE sp_calculate_distance();

-- FK
ALTER TABLE poi ADD CONSTRAINT poi_fk_track FOREIGN KEY (track_id) REFERENCES track(id) ON DELETE CASCADE;
ALTER TABLE video ADD CONSTRAINT video_fk_track FOREIGN KEY (track_id) REFERENCES track(id) ON DELETE CASCADE;
ALTER TABLE picture ADD CONSTRAINT picture_fk_track FOREIGN KEY (track_id) REFERENCES track(id) ON DELETE CASCADE;
ALTER TABLE comment ADD CONSTRAINT comment_fk_track FOREIGN KEY (track_id) REFERENCES track(id) ON DELETE CASCADE;

--
-- Name: track track_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY track
ADD CONSTRAINT track_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);

-- +migrate StatementEnd

-- +migrate Down
DROP DATABASE 'la-trace';