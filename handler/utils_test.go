package handler

import (
	"testing"
)

// func TestFormatFilterAsQuery(t *testing.T) {
// 	v := url.Values{}
// 	v.Set("dist_min", "10")
// 	v.Add("dist_max", "100")
// 	v.Add("ele_max", "1000")
// 	v.Add("activity", "mtb")

// 	s := v.Encode()
// 	t.Log(s)

// 	result := "distance < 100 AND distance > 10 AND elevation <= 1000 AND activity = 'mtb'"

// 	if formatFilterAsQuery(s) != result {
// 		t.Errorf("formatted query is not correct, %s != %s", formatFilterAsQuery(s), result)
// 	}
// }

func TestFormatCoordinates(t *testing.T) {

	cases := map[string]string{
		"5.0000001": "5.0000",
		"5.00":      "5.0000",
		"5":         "5.0000",
	}

	for k, v := range cases {
		test, err := formatCoordinates(k)

		if err != nil {
			t.Fatal(err)
		}

		if test != v {
			t.Errorf("Format is not correct expected %s, but get %s", v, test)
		}
	}

}
