package handler

import (
	"bytes"
	"fmt"
	"image"
	"io/ioutil"
	// dealing with jpeg and png
	_ "image/jpeg"
	_ "image/png"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/xor-gate/goexif2/exif"
	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/storage"
)

var allowedFileTypes = map[string]string{
	"image/jpeg": ".jpg",
	"image/jpg":  ".jpg",
	"image/png":  ".png",
}

const (
	headerContentType = "Content-Type"
	formFileFieldName = "file"
)

func (h *Handler) GetPictures(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	pix, err := h.Dao.GetPicturesByTrackID(id)

	if err != nil {
		fmt.Println(err)
	}

	api.JSON(w, pix)
}

func (h *Handler) CreatePictures(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var (
		lat    float64
		lon    float64
		width  int
		height int
	)

	r.ParseMultipartForm(32 << 20)
	file, fileHeader, err := r.FormFile(formFileFieldName)

	if err != nil {
		fmt.Println(err)
		return
	}

	fileBytes, err := ioutil.ReadAll(file)

	defer file.Close()

	if err != nil {
		fmt.Println(err)
		return
	}

	// Checking type
	t := fileHeader.Header.Get(headerContentType)

	fileExtension, ok := isAllowedType(t)

	if !ok {
		api.JSONWithHTTPCode(w, errorBadParam.payload(), http.StatusUnsupportedMediaType)
	}

	// Get size of the image
	config, _, err := image.DecodeConfig(bytes.NewReader(fileBytes))
	if err != nil {
		fmt.Printf("w: %d\n", err)
	} else {
		width = config.Width
		height = config.Height
	}

	// Read the exifs
	exifData, err := exif.Decode(bytes.NewReader(fileBytes))
	if err != nil {
		log.Println("error : ", err)
	} else {
		lat, lon, err = exifData.LatLong()
	}

	// store in database
	p, err := h.Dao.InsertPicture(width, height, lon, lat, id, fileExtension)
	p.Width = width
	p.Height = height

	// create the target file
	path := h.Paths["pictures"] + p.ID + fileExtension
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	// Copy the content
	_, err = io.Copy(f, bytes.NewReader(fileBytes))
	if err != nil {
		fmt.Println(err)
	}

	// Response payload
	api.JSONWithHTTPCode(w, map[string]string{"id": p.ID}, http.StatusCreated)
}

func (h *Handler) DeletePictures(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	query := `DELETE FROM picture WHERE id = $1
	RETURNING CONCAT(id, file_extension)`

	result := h.DB.QueryRow(query, id)

	fileName := ""
	err := result.Scan(&fileName)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorDatabase.payload(), errorDatabase.code)
		return
	}

	// create the target file
	path := h.Paths["pictures"] + fileName
	err = os.Remove(path)

	if err != nil {
		fmt.Println(err)
		return
	}

	api.JSONWithHTTPCode(w, nil, http.StatusNoContent)
}

func (h *Handler) PatchPictures(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	p := storage.EditablePicture{}

	err := api.GetJSONContent(&p, r)

	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.Dao.PatchPicture(id, p)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	api.JSON(w, p)
}

func isAllowedType(t string) (string, bool) {
	extension, ok := allowedFileTypes[t]
	return extension, ok
}
