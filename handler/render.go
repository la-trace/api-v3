package handler

import (
	"bytes"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/la-trace/api-v3/api"
)

func (h *Handler) Render(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	trackID := vars["trackID"]

	content, err := h.Dao.GetTrackAsGeoJSONByIDAndFactor(trackID, 0.0001)

	if err != nil {
		api.Error(w, "Error while downloading the track", http.StatusInternalServerError)
		return
	}
	geojson, err := json.Marshal(content)

	if err != nil {
		api.Error(w, "Error while marshalling the track", http.StatusInternalServerError)
		return
	}

	req, err := http.NewRequest("POST", "/?w=680&h=512&stroke=3", bytes.NewReader(geojson))

	if err != nil {
		api.Error(w, "Error while creatong the request", http.StatusInternalServerError)
		return
	}

	req.Header.Set("Content-Type", "application/json")
	h.renderProxy.ServeHTTP(w, req)
}
