-- +migrate Up
-- +migrate StatementBegin

CREATE OR REPLACE FUNCTION BBox(x integer, y integer, zoom integer)
    RETURNS geometry AS
$BODY$
DECLARE
    max numeric := 6378137 * pi();
    res numeric := max * 2 / 2^zoom;
    bbox geometry;
BEGIN
    return ST_MakeEnvelope(
        -max + (x * res),
        max - (y * res),
        -max + (x * res) + res,
        max - (y * res) - res,
        3857);
END;
$BODY$
LANGUAGE plpgsql IMMUTABLE;

CREATE TABLE gr (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text,
    descrption text,
    filename text,
    linestring geometry
);

ALTER TABLE gr ADD CONSTRAINT gr_pk PRIMARY KEY (id);
CREATE INDEX gr_linestring_idx ON gr USING gist (linestring);

-- +migrate StatementEnd

-- +migrate Down
DROP TABLE gr;
DROP FUNCTION BBox;