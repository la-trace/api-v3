package model

import (
	"errors"
	"net/http"
)

const (

	/* Error types */

	// TypeValidationError is used when data cannot be validated
	TypeValidationError = "VALIDATION_ERROR"

	// TypeInternalServerError is used for unexpected internal error, such as no database access
	TypeInternalServerError = "INTERNAL_SERVER_ERROR"

	// TypeServiceUnavailableError is used a service is unavailable
	TypeServiceUnavailableError = "SERVICE_UNAVAILABLE_ERROR"

	// TypeAuthenticationError is used if authentication is not successful
	TypeAuthenticationError = "AUTHENTICATION_ERROR"

	// TypeAuthorizationError is used if authorization is not granted for that data
	TypeAuthorizationError = "AUTHORIZATION_ERROR"

	// TypeParameterError is used for wrong parameter value
	TypeParameterError = "PARAMETER_ERROR"

	// TypeNotFoundError is used for not found errors
	TypeNotFoundError = "NOT_FOUND"
)

var (
	/* Error list */

	// ErrNotFound is thrown when no data is found
	ErrNotFound = errors.New("not found error")

	// ErrNoValidatorDefined is thrown when no validator is defined when saving data
	ErrNoValidatorDefined = errors.New("not validator defined error")

	// ErrBadConfig is thrown when configuration is wrong or not existent
	ErrBadConfig = errors.New("bad configuration error")

	// ErrBadRequest is thrown when the request cannot not be answered with the given parameters
	ErrBadRequest = errors.New("bad request error")

	// ErrInternalServerError is thrown when an unexpected internal error happens like a database connection is closed
	ErrInternalServerError = errors.New("internal server error")

	/* Error definitions */

	//  ----------------- TOKEN ERRORS (401, 403) ----------------- //

	// MsgMissingAuth is the error definition for the missing token
	MsgMissingAuth = ErrorDefinition{
		Type:     TypeAuthenticationError,
		Message:  "Missing Authorization",
		Tip:      "Add Authorization Header with the token to your request",
		HTTPCode: http.StatusUnauthorized,
	}

	// MsgTokenIsUnknown token can't be found in the database
	MsgTokenIsUnknown = ErrorDefinition{
		Type:     TypeAuthenticationError,
		Message:  "Token is unknown",
		Tip:      "Make sure that the token is correct",
		HTTPCode: http.StatusUnauthorized,
	}

	// MsgTokenHasExpired is the error definition for expired token
	MsgTokenHasExpired = ErrorDefinition{
		Type:     TypeAuthorizationError,
		Message:  "Token has expired",
		Tip:      "Refresh the access token and then retry",
		HTTPCode: http.StatusForbidden,
	}

	// MsgTokenIsRevoked is the error definition for revoked token
	MsgTokenIsRevoked = ErrorDefinition{
		Type:     TypeAuthorizationError,
		Message:  "Token has been revoked",
		Tip:      "Create a new access token and then retry",
		HTTPCode: http.StatusForbidden,
	}

	// MsgTokenMalformed is the error definition for malformed token
	MsgTokenMalformed = ErrorDefinition{
		Type:     TypeAuthorizationError,
		Message:  "Authorization header is not valid",
		Tip:      "Make sure that the header has a valid format",
		HTTPCode: http.StatusUnauthorized,
	}

	// 400

	// MsgBadParameter is the error definition for an invalid paramater
	MsgBadParameter = ErrorDefinition{
		Type:     TypeValidationError,
		Message:  "A least one paramater is invalid",
		Tip:      "Please check your parameters or payload",
		HTTPCode: http.StatusBadRequest,
	}

	// 404

	// MsgEntityNotFound is the error definition for entity not found
	MsgEntityNotFound = ErrorDefinition{
		Type:     TypeNotFoundError,
		Message:  "Entity doesn't exist",
		Tip:      "Please check the identifier",
		HTTPCode: http.StatusNotFound,
	}

	// MsgEndpointDoesNotExist is the error definition for user not found
	MsgEndpointDoesNotExist = ErrorDefinition{
		Type:     TypeNotFoundError,
		Message:  "Endpoint doesn't exist",
		Tip:      "Please check your url - https://media.giphy.com/media/jcxtvm2bsZDH2/giphy.gif",
		HTTPCode: http.StatusNotFound,
	}

	// 500

	// MsgInternalServerError is the error definition for internal server error
	MsgInternalServerError = ErrorDefinition{
		Type:     TypeInternalServerError,
		Message:  "Error occured on server",
		Tip:      "Please light a candle and pray for our devops",
		HTTPCode: http.StatusInternalServerError,
	}
)

// ErrorDefinition defines an error message and associated tip
type ErrorDefinition struct {
	Type     string `json:"type"`
	Message  string `json:"message"`
	Tip      string `json:"tip"`
	HTTPCode int    `json:"-"`
}
