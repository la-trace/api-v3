
-- +migrate Up
ALTER TABLE track ALTER COLUMN downloads SET DEFAULT 0;
UPDATE track SET downloads = 0 WHERE downloads IS NULL;
ALTER TABLE track ALTER COLUMN downloads SET NOT NULL;

-- +migrate Down
ALTER TABLE track ALTER COLUMN downloads DROP DEFAULT;
ALTER TABLE track ALTER COLUMN downloads DROP NOT NULL;