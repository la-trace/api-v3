package storage

import "testing"

func TestCheckPostGisVersion(t *testing.T) {
	type args struct {
		v string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Good Version",
			args: args{
				v: `"POSTGIS="2.5.2 r17328" [EXTENSION] PGSQL="100" GEOS="3.5.1-CAPI-1.9.1 r4246" PROJ="Rel. 4.9.3, 15 August 2016" GDAL="GDAL 2.1.2, released 2016/10/24" LIBXML="2.9.4" LIBJSON="0.12.1" LIBPROTOBUF="1.2.1" TOPOLOGY RASTER"`,
			},
			want: true,
		},
		{
			name: "Good 2.4 Version",
			args: args{
				v: `"POSTGIS="2.4.0 r17328" [EXTENSION] PGSQL="100" GEOS="3.5.1-CAPI-1.9.1 r4246" PROJ="Rel. 4.9.3, 15 August 2016" GDAL="GDAL 2.1.2, released 2016/10/24" LIBXML="2.9.4" LIBJSON="0.12.1" LIBPROTOBUF="1.2.1" TOPOLOGY RASTER"`,
			},
			want: true,
		},
		{
			name: "Good Futuristic Version",
			args: args{
				v: `"POSTGIS="3.15.145 r17328" [EXTENSION] PGSQL="100" GEOS="3.5.1-CAPI-1.9.1 r4246" PROJ="Rel. 4.9.3, 15 August 2016" GDAL="GDAL 2.1.2, released 2016/10/24" LIBXML="2.9.4" LIBJSON="0.12.1" LIBPROTOBUF="1.2.1" TOPOLOGY RASTER"`,
			},
			want: true,
		},
		{
			name: "Bad version",
			args: args{
				v: `"POSTGIS="2.3.2 r17328" [EXTENSION] PGSQL="100" GEOS="3.5.1-CAPI-1.9.1 r4246" PROJ="Rel. 4.9.3, 15 August 2016" GDAL="GDAL 2.1.2, released 2016/10/24" LIBXML="2.9.4" LIBJSON="0.12.1" LIBPROTOBUF="1.2.1" TOPOLOGY RASTER"`,
			},
			want: false,
		},
		{
			name: "Shitty Input",
			args: args{
				v: "%$<1&<%+=!\"83?+)9:++9138 42/ \"7;0-,)06 \"1(2;6>?99$%7!!*#96=>2&-/(5*)=$;0$$+;<12",
			},
			want: false,
		},
		{
			name: "Empty Input",
			args: args{
				v: "",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CheckPostGisVersion(tt.args.v); got != tt.want {
				t.Errorf("TestPostGisVersion() = %v, want %v", got, tt.want)
			}
		})
	}
}
