package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/la-trace/api-v3/api"
)

func (h *Handler) GRTiles(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	x, err := strconv.Atoi(vars["x"])
	if err != nil {

	}
	y, err := strconv.Atoi(vars["y"])
	if err != nil {

	}
	z, err := strconv.Atoi(vars["z"])
	if err != nil {

	}

	data, err := h.Dao.GetTiles(x, y, z)

	if err != nil {
		api.Error(w, "Error while generating MVT tile", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/vnd.mapbox-vector-tile")
	w.Header().Set("Cache-Control", "max-age=2592000") // 30 days
	fmt.Fprintf(w, "%s", data)
}
