package postgres

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"math"

	"gitlab.com/la-trace/api-v3/storage"
	// pg
	pq "github.com/lib/pq"
	"github.com/nleof/goyesql"
	geojson "github.com/paulmach/go.geojson"
)

func Round(f float64) float64 {
	return math.Floor(f + .5)
}
func RoundPlus(f float64, places int) float64 {
	shift := math.Pow(10, float64(places))
	return Round(f*shift) / shift
}

// Postgres storage type
type Postgres struct {
	DB      *sql.DB
	Queries goyesql.Queries
	Salt    string
}

// New is a postgres storage constructor
func New(c, q, salt string) (storage.Storage, error) {
	db, err := sql.Open("postgres", c)
	return &Postgres{
		DB:      db,
		Queries: goyesql.MustParseFile(q),
		Salt:    salt,
	}, err
}

func (p *Postgres) IncNumberOfDownloadsOfTrack(id string) error {
	q := p.Queries["incDownload"]
	_, err := p.DB.Exec(q, id)
	fmt.Println(err)
	switch {
	case err == sql.ErrNoRows:
		return storage.ErrNoRows
	case err != nil:
		return err
	}
	return nil
}

func (p *Postgres) GetTrackByID(id string) (*storage.Track, error) {
	start := []byte{}
	end := []byte{}
	rating := []byte{}
	t := storage.Track{}
	q := p.Queries["getTrack"]
	err := p.DB.QueryRow(q, id).Scan(
		&t.ID, &t.Name, &t.Description, &t.ElevationGain, &t.ElevationLoss, &t.Creation, &t.Distance, &start, &end, &rating, &t.Downloads,
		&t.User.ID, &t.User.Name, &t.User.Creation, &t.User.Modification)

	switch {
	case err == sql.ErrNoRows:
		return nil, storage.ErrNoRows
	case err != nil:
		return nil, err
	}

	t.StartAddress = &storage.Address{}
	t.EndAddress = &storage.Address{}
	err = t.StartAddress.FromJSONString(start)
	if err != nil {
		return nil, err
	}

	t.EndAddress.FromJSONString(end)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(rating, &t.Rating)

	switch {
	case err == sql.ErrNoRows:
		return nil, storage.ErrNoRows
	case err != nil:
		return nil, err
	default:
		return &t, err
	}
}

func (p *Postgres) GetPicturesByTrackID(id string) ([]storage.Picture, error) {
	pix := []storage.Picture{}
	q := p.Queries["getPictures"]
	rows, err := p.DB.Query(q, id)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		p := storage.NewPicture()
		err = rows.Scan(&p.ID, &p.Creation, &p.Caption, &p.Width, &p.Height, &p.URL, &p.Position)
		pix = append(pix, p)
	}

	return pix, nil
}

func (p *Postgres) GetVideosByTrackID(id string) ([]storage.Video, error) {
	videos := []storage.Video{}
	q := p.Queries["getVideos"]
	rows, err := p.DB.Query(q, id)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		v := storage.Video{}
		err = rows.Scan(&v.ID, &v.Creation, &v.Modification, &v.Platform, &v.Slug)
		videos = append(videos, v)
	}

	return videos, nil
}

func (p *Postgres) GetCommentsByTrackID(id string) ([]storage.Comment, error) {
	comments := []storage.Comment{}
	q := p.Queries["getComments"]
	rows, err := p.DB.Query(q, id)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		cmt := storage.Comment{}
		err = rows.Scan(&cmt.ID, &cmt.Creation, &cmt.Modification, &cmt.Content, &cmt.User.ID, &cmt.User.Name, &cmt.User.Creation, &cmt.User.Modification)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		comments = append(comments, cmt)
	}

	return comments, nil
}

func (p *Postgres) GetPoisByTrackID(id string) ([]storage.Poi, error) {
	pois := []storage.Poi{}
	q := p.Queries["getPois"]
	rows, err := p.DB.Query(q, id)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		p := storage.NewPoi()
		err = rows.Scan(&p.ID, &p.Caption, &p.Modification, &p.Creation, &p.Icon, &p.Position)
		fmt.Printf("%v %s\n", p, err)
		pois = append(pois, p)
	}

	return pois, nil
}

func (p *Postgres) GetTrackAsGeoJSONByIDAndFactor(id string, f float64) (*geojson.Geometry, error) {
	var geometry *geojson.Geometry
	q := p.Queries["getTrackAsGeoJSON"]
	err := p.DB.QueryRow(q, f, id).Scan(&geometry)

	switch {
	case err == sql.ErrNoRows:
		return nil, storage.ErrNoRows
	case err != nil:
		return nil, err
	default:
		return geometry, nil
	}
}

func (p *Postgres) GetTrackIDNearLonLat(lon, lat float64) ([]storage.NearTrack, error) {
	tracks := []storage.NearTrack{}
	q := p.Queries["getNearLonLat"]
	rows, err := p.DB.Query(q, lon, lat, 20)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		t := storage.NearTrack{}
		err = rows.Scan(&t.Distance, &t.ID, &t.Name, &t.Description)
		tracks = append(tracks, t)
	}

	return tracks, nil
}

func (p *Postgres) GetTrackElevationByID(id string) ([]storage.Elevation, error) {
	ele := []storage.Elevation{}
	q := p.Queries["getElevation"]
	rows, err := p.DB.Query(q, id)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		elevation := 0.0
		distance := 0.0
		lon := 0.0
		lat := 0.0
		rows.Scan(&distance, &elevation, &lon, &lat)
		ele = append(ele, storage.Elevation{elevation, distance, lon, lat})
	}

	return ele, nil
}

func (p *Postgres) GetIntersectsTracksAsCentroidGeoJSON(swLon, swLat, neLon, neLat float64, whereClause string) ([]storage.TrackAsPoint, error) {
	tracks := []storage.TrackAsPoint{}
	q := p.Queries["getIntersectingTracksAsGeoJSONPoint"]
	rows, err := p.DB.Query(q+whereClause, swLon, swLat, neLon, neLat)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		pt := storage.TrackAsPoint{}
		rows.Scan(&pt.ID, &pt.Point)
		if pt.ID != "" {
			tracks = append(tracks, pt)
		}
	}

	return tracks, nil
}

func (p *Postgres) GetIntersectingTracksId(swLon, swLat, neLon, neLat float64, whereClause string) ([]string, error) {
	IDs := []string{}
	q := p.Queries["getIntersectingTracksId"]
	rows, err := p.DB.Query(q+whereClause, swLon, swLat, neLon, neLat)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id string
		rows.Scan(&id)
		IDs = append(IDs, id)
	}

	return IDs, nil
}

func (p *Postgres) Login(login, password string) (*storage.User, error) {
	q := p.Queries["login"]
	u := storage.User{}
	err := p.DB.QueryRow(q, login, p.Salt+password).Scan(&u.ID, &u.Name, &u.MySQLID, &u.ValidationID)

	if err != nil {
		return nil, err
	}
	return &u, nil
}

func (p *Postgres) InsertPicture(width, height int, lon, lat float64, id, fileExtension string) (*storage.Picture, error) {
	q := p.Queries["insertPicture"]
	result := p.DB.QueryRow(q, width, height, lon, lat, id, fileExtension)
	pic := storage.NewPicture()
	err := result.Scan(&pic.ID)
	return &pic, err
}

func (p *Postgres) DeletePoi(id string) error {
	q := p.Queries["deletePoi"]
	_, err := p.DB.Exec(q, id)
	return err
}

func (p *Postgres) DeleteTrack(trackId string) error {
	q := p.Queries["deleteTrack"]
	_, err := p.DB.Exec(q, trackId)
	return err
}

func (p *Postgres) InsertPoi(caption, trackID string, lon float64, lat float64) (*storage.Poi, error) {
	q := p.Queries["insertPoi"]
	result := p.DB.QueryRow(q, caption, trackID, lon, lat)
	poi := storage.Poi{}
	err := result.Scan(&poi.ID)
	return &poi, err
}

func (p *Postgres) InsertVideo(platform, slug, trackID string) (*storage.Video, error) {
	q := p.Queries["insertVideo"]
	result := p.DB.QueryRow(q, platform, slug, trackID)
	video := storage.Video{}
	err := result.Scan(&video.ID)
	return &video, err
}

func (p *Postgres) DeleteVideo(trackID string) error {
	q := p.Queries["deleteVideo"]
	_, err := p.DB.Exec(q, trackID)
	return err
}

func (p *Postgres) PatchTrack(id string, t storage.EditableTrack) error {
	q := p.Queries["patchTrack"]
	ps := t.Patch()
	q = fmt.Sprintf(q, ps.Fields, ps.Placeholders, ps.Cursor)
	fmt.Println(q)
	v := append(ps.Values, id)
	_, err := p.DB.Exec(q, v...)
	return err
}

func (p *Postgres) PatchPoi(id string, t storage.EditablePoi) error {
	q := p.Queries["patchPoi"]
	ps := t.Patch()
	q = fmt.Sprintf(q, ps.Fields, ps.Placeholders, ps.Cursor)
	v := append(ps.Values, id)
	_, err := p.DB.Exec(q, v...)
	return err
}

func (p *Postgres) PatchPicture(id string, pix storage.EditablePicture) error {
	q := p.Queries["patchPictures"]
	ps := pix.Patch()
	q = fmt.Sprintf(q, ps.Fields, ps.Placeholders, ps.Cursor)
	v := append(ps.Values, id)
	_, err := p.DB.Exec(q, v...)
	return err
}

func (p *Postgres) PatchUser(id string, u storage.EditableUser) error {
	q := p.Queries["patchUser"]
	usr := u.Patch()
	q = fmt.Sprintf(q, usr.Fields, usr.Placeholders, usr.Cursor)
	v := append(usr.Values, id)
	_, err := p.DB.Exec(q, v...)
	return err
}

func (p *Postgres) UpdatePassword(password, id string) error {
	q := p.Queries["updatePassword"]
	_, err := p.DB.Exec(q, p.Salt+password, id)
	return err
}

func (p *Postgres) AddUser(u storage.AddingUser) (*storage.User, error) {
	q := p.Queries["addUser"]

	user := storage.User{}
	err := p.DB.QueryRow(q, u.Name, u.Email, p.Salt+u.Password).Scan(&user.ID, &user.Name, &user.Creation)

	if pgerr, ok := err.(*pq.Error); ok {
		if pgerr.Code == "23505" {
			return nil, storage.ErrEmailAlreadyExists
		}
	}

	return &user, err
}

func (p *Postgres) GetStarredTracks() ([]storage.Starred, error) {
	starred := []storage.Starred{}
	q := p.Queries["getStarred"]
	rows, err := p.DB.Query(q)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		start := []byte{}
		s := storage.Starred{}
		rows.Scan(&s.ID, &s.Distance, &s.ElevationGain, &s.Name, &s.Picture, &start)
		s.StartAddress = &storage.Address{}
		s.StartAddress.FromJSONString(start)
		starred = append(starred, s)
	}

	return starred, nil
}

func (p *Postgres) InsertComment(content, trackID, userID string) error {
	q := p.Queries["addComment"]
	_, err := p.DB.Exec(q, content, trackID, userID)
	return err
}

func (p *Postgres) InsertMagicLink(code, userID, action string) error {
	q := p.Queries["addMagicLink"]
	_, err := p.DB.Exec(q, code, userID, action)
	return err
}

func (p *Postgres) GetUserIDByEmail(email string) (uuid string, err error) {
	q := p.Queries["getUserIdByEmail"]
	err = p.DB.QueryRow(q, email).Scan(&uuid)
	return uuid, err
}

func (p *Postgres) SetMagicLinkConsumed(code string) error {
	q := p.Queries["setMagicLinkConsumed"]
	_, err := p.DB.Exec(q, code)
	return err
}

func (p *Postgres) GetMagicLinkByCode(code string) (*storage.MagicLink, error) {
	m := storage.MagicLink{}
	q := p.Queries["getMagicLinkByCode"]
	err := p.DB.QueryRow(q, code).Scan(&m.Code, &m.CreatedAt, &m.ConsumedAt, &m.UserID, &m.Code, &m.Action)

	switch {
	case err == sql.ErrNoRows:
		return nil, storage.ErrNoRows
	case err != nil:
		return nil, err
	default:
		return &m, nil
	}
}

func (p *Postgres) SetEmailValidated(validationID, userID string) error {
	q := p.Queries["validateEmailByUserID"]
	_, err := p.DB.Exec(q, validationID, userID)
	return err
}

func (p *Postgres) LogDownload(userID, trackID string) error {
	q := p.Queries["logDowload"]
	_, err := p.DB.Exec(q, userID, trackID)
	return err
}

func (p *Postgres) GetAllTrackID() ([]storage.SiteMapTrack, error) {
	q := p.Queries["getAlltrackID"]
	tracks := []storage.SiteMapTrack{}
	rows, err := p.DB.Query(q)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var t storage.SiteMapTrack
		rows.Scan(&t.ID)
		tracks = append(tracks, t)
	}

	return tracks, nil
}

func (p *Postgres) GetHeatMap(swLon, swLat, neLon, neLat float64) (storage.HeatMap, error) {
	q := p.Queries["heatmap"]
	hm := storage.HeatMap{}
	rows, err := p.DB.Query(q, swLon, swLat, neLon, neLat)

	// @TODO Better error handling
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var p storage.HeatMapPoint
		rows.Scan(&p[0], &p[1], &p[2])
		hm = append(hm, p)
	}

	return hm, nil
}

func (p *Postgres) GetUserHeatMap(userID string) (storage.HeatMap, error) {
	q := p.Queries["userHeatmap"]
	hm := storage.HeatMap{}
	rows, err := p.DB.Query(q, userID)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var p storage.HeatMapPoint
		rows.Scan(&p[0], &p[1], &p[2])
		hm = append(hm, p)
	}

	return hm, nil
}

func (p *Postgres) GetTrackIDFromMySQLID(id string) (uuid string, err error) {
	q := p.Queries["getIdFromMySQLID"]
	err = p.DB.QueryRow(q, id).Scan(&uuid)
	switch {
	case err == sql.ErrNoRows:
		return "", storage.ErrNoRows
	case err != nil:
		return "", err
	}

	return uuid, err
}

func (p *Postgres) GetTiles(x, y, z int) ([]byte, error) {
	q := p.Queries["getTiles"]
	data := []byte{}
	err := p.DB.QueryRow(q, x, y, z).Scan(&data)
	return data, err
}

func (p *Postgres) CheckPostGisVersion() (bool, error) {
	q := p.Queries["getPostGisVersion"]
	data := ""
	err := p.DB.QueryRow(q).Scan(&data)
	if err != nil {
		return false, err
	}
	return storage.CheckPostGisVersion(data), nil
}

func (p *Postgres) GetTracksByUserID(userID string, limit, offset string) ([]storage.Track, string, error) {
	tracks := []storage.Track{}
	q := p.Queries["getTracksByUserID"]
	rows, err := p.DB.Query(q, userID, limit, offset)

	// @TODO Better error handling
	if err != nil {
		return nil, "", err
	}
	defer rows.Close()
	var count string
	for rows.Next() {
		t := storage.Track{}
		rows.Scan(&t.ID, &t.Name, &t.Description, &t.ElevationGain, &t.ElevationLoss, &t.Creation, &t.Distance, &t.Downloads, &t.User.ID, &t.User.Name, &t.User.Creation, &t.User.Modification, &count)
		tracks = append(tracks, t)
	}

	return tracks, count, nil
}

// func populateTrack(t storage.Track) (storage.Track, error) {
// 	pix, err = h.Dao.GetPicturesByTrackID(id)
// 	if err != nil {
// 		return err
// 	}
// 	t.Pictures = pix
// 	videos, err := h.Dao.GetVideosByTrackID(id)
// 	if err != nil {
// 		return err
// 	}
// 	t.Videos = videos
// 	comments, err := h.Dao.GetCommentsByTrackID(id)
// 	if err != nil {
// 		return err
// 	}
// 	t.Comments = comments
// 	pois, err := h.Dao.GetPoisByTrackID(id)
// 	if err != nil {
// 		return err
// 	}
// 	t.Pois = pois

// 	return track
// }
