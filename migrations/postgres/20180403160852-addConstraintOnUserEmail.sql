
-- +migrate Up
ALTER TABLE "user" ADD CONSTRAINT ak_email UNIQUE (email);

-- +migrate Down
ALTER TABLE "user" DROP CONSTRAINT IF EXISTS ak_email;
