-- +migrate Up
CREATE TABLE download (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    user_id uuid,
    track_id uuid
);

ALTER TABLE download ADD CONSTRAINT download_pkey PRIMARY KEY (id);
ALTER TABLE download ADD CONSTRAINT download_fk_user FOREIGN KEY (user_id) REFERENCES "user"(id);
ALTER TABLE download ADD CONSTRAINT download_fk_track FOREIGN KEY (track_id) REFERENCES "track"(id);

-- +migrate Down
DROP TABLE download;