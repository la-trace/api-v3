# la-trace.com API

[![pipeline status](https://gitlab.com/la-trace/api-v3/badges/master/pipeline.svg)](https://gitlab.com/la-trace/api-v3/commits/master)

## Migrations

Project uses https://github.com/rubenv/sql-migrate

    cd migrations

### Play migrations

    sql-migrate up

### Create migration

    sql-migrate new theNameOfTheMigration