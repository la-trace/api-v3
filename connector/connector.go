package connector

import "net/http"

type Access struct {
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
}

type Connector interface {
	Connect(w http.ResponseWriter, r *http.Request)
	Callback(w http.ResponseWriter, r *http.Request)
}
