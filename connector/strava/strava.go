package strava

import (
	"context"
	"net/http"

	"gitlab.com/la-trace/api-v3/api"

	"gitlab.com/la-trace/api-v3/connector"
	"golang.org/x/oauth2"
)

var authURL string = "https://www.strava.com/oauth/authorize"
var tokenURL string = "https://www.strava.com/oauth/token"

type Strava struct {
	config *oauth2.Config
}

func New(clientID, clientSecret, redirectURL string) connector.Connector {

	conf := &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Scopes:       []string{"read,read_all,activity:read_all"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  authURL,
			TokenURL: tokenURL,
		},
		RedirectURL: redirectURL,
	}

	return &Strava{
		config: conf,
	}
}

func (s *Strava) Connect(w http.ResponseWriter, r *http.Request) {
	url := s.config.AuthCodeURL("state", oauth2.AccessTypeOffline)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (s *Strava) Callback(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	code := r.URL.Query().Get("code")
	tok, _ := s.config.Exchange(ctx, code)

	payload := connector.Access{
		Token:        tok.AccessToken,
		RefreshToken: tok.RefreshToken,
	}

	api.JSON(w, payload)
}
