package handler

import (
	"bytes"
	"io/ioutil"
	"path/filepath"
	"testing"

	_ "github.com/lib/pq"
	"gitlab.com/la-trace/api-v3/storage/mock"
)

func HandlerFactory() Handler {
	d := mock.Mock{}
	return Handler{
		templateList: templateList{},
		Dao:          &d,
	}
}

func helperLoadBytes(t *testing.T, name string) []byte {
	path := filepath.Join("testdata", name) // relative path
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}
	return bytes
}

func TestHandler_RegisterTemplate(t *testing.T) {
	type args struct {
		n      templateName
		l      locale
		path   string
		result string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Main test",
			args: args{
				n:      "foo",
				l:      "fr_FR",
				path:   filepath.Join("testdata", "template-test.html"),
				result: "Hello world",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := HandlerFactory()

			// RegisterTemplate should not returns error
			if err := h.RegisterTemplate(tt.args.n, tt.args.l, tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("Handler.RegisterTemplate() error = %v, wantErr %v", err, tt.wantErr)
			}

			// templateList should have a size of 1
			if len(h.templateList) != 1 {
				t.Errorf("Handler.templateList must be length of 1, %d", len(h.templateList))
			}

			// RegisterTemplate should contains the added template
			if _, ok := h.templateList[tt.args.n]; !ok {
				t.Errorf("Template name %s, should exists", tt.args.n)
			}

			// RegisterTemplate should contains the added template with the locale
			if _, ok := h.templateList[tt.args.n][tt.args.l]; !ok {
				t.Errorf("Template name %s, should contains a locale %s", tt.args.n, tt.args.l)
			}

			// Testing render of template
			var tpl bytes.Buffer
			testTemplate := h.templateList[tt.args.n][tt.args.l]
			data := struct {
				Value string
			}{
				Value: "world",
			}

			_ = testTemplate.Execute(&tpl, data)
			if tpl.String() != tt.args.result {
				t.Errorf("Rendered `%s`, different from  `%s`", tpl.String(), tt.args.result)
			}
		})
	}
}
