module gitlab.com/la-trace/api-v3

go 1.14

require (
	github.com/beorn7/perks v0.0.0-20160804104726-4c0e84591b9a
	github.com/dgrijalva/jwt-go v3.1.0+incompatible
	github.com/go-playground/locales v0.12.1
	github.com/go-playground/universal-translator v0.16.0
	github.com/golang/protobuf v1.3.1
	github.com/gorilla/context v0.0.0-20160226214623-1ea25387ff6f
	github.com/gorilla/mux v1.6.1
	github.com/inconshreveable/mousetrap v1.0.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/lib/pq v0.0.0-20180123210206-19c8e9ad0095
	github.com/matttproud/golang_protobuf_extensions v1.0.0
	github.com/meatballhat/negroni-logrus v0.0.0-20170801195057-31067281800f
	github.com/nleof/goyesql v1.0.1
	github.com/paulmach/go.geojson v1.3.1
	github.com/prometheus/client_golang v0.8.0
	github.com/prometheus/client_model v0.0.0-20171117100541-99fa1f4be8e5
	github.com/prometheus/common v0.0.0-20180110214958-89604d197083
	github.com/prometheus/procfs v0.0.0-20180125133057-cb4147076ac7
	github.com/ptrv/go-gpx v0.0.0-20151216232927-c93804873f99
	github.com/rs/cors v0.0.0-20170727213201-7af7a1e09ba3
	github.com/rs/xid v1.2.1
	github.com/rubenv/sql-migrate v0.0.0-20180217203553-081fe17d19ff
	github.com/sendgrid/rest v2.4.0+incompatible
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible
	github.com/sirupsen/logrus v1.0.4
	github.com/snabb/diagio v1.0.0
	github.com/snabb/sitemap v1.0.0
	github.com/spf13/cobra v0.0.1
	github.com/spf13/pflag v1.0.0
	github.com/umahmood/haversine v0.0.0-20151105152445-808ab04add26
	github.com/urfave/negroni v0.3.0
	github.com/xor-gate/goexif2 v1.1.0
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/sys v0.0.0-20190606165138-5da285871e9c
	golang.org/x/text v0.3.2
	google.golang.org/appengine v1.6.1
	gopkg.in/go-playground/validator.v9 v9.13.0
	gopkg.in/gorp.v1 v1.7.1
)
