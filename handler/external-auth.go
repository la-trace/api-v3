package handler

import (
	"net/http"

	"gitlab.com/la-trace/api-v3/api"
)

func (h *Handler) Auth(w http.ResponseWriter, r *http.Request) {
	api.JSONWithHTTPCode(w, nil, http.StatusNoContent)
}
