package handler

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

type queryMap struct {
	QueryString string
	Field       string
	Operator    string
	IsString    bool
	Value       string
}

func (q queryMap) toSQL() string {
	if q.IsString {
		return fmt.Sprintf("%s %s '%s'", q.Field, q.Operator, q.Value)
	}
	return fmt.Sprintf("%s %s %s", q.Field, q.Operator, q.Value)
}

var keyMapping = []queryMap{
	{
		"dist_max",
		"track.distance",
		"<",
		false,
		"",
	},
	{
		"dist_min",
		"track.distance",
		">",
		false,
		"",
	},
	{
		"ele_max",
		"track.elevation_gain",
		"<=",
		false,
		"",
	},
	{
		"activity",
		"track.activity",
		"=",
		true,
		"",
	},
}

func formatFilterAsQuery(s string) string {

	sqlQuery := []string{}

	v, _ := url.ParseQuery(s)

	for _, qm := range keyMapping {
		value := v.Get(qm.QueryString)

		if value != "" && value != "0" {
			qm.Value = value
			sqlQuery = append(sqlQuery, qm.toSQL())
		}

	}

	return strings.Join(sqlQuery, " AND ")
}

func formatCoordinates(coord string) (fc string, e error) {

	coordf, err := strconv.ParseFloat(coord, 64)

	if err != nil {
		return fc, err
	}

	fc = fmt.Sprintf("%.4f", coordf)

	return fc, nil
}
