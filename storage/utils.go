package storage

import (
	"errors"
	"fmt"
	"math"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

type Fields []string
type Values []interface{}

var postgisRegexp = regexp.MustCompile(`POSTGIS="(\d\.\d{1,}\.\d{1,})`)

func (f Fields) GetColumns() string {
	return strings.Join(f, ", ")
}

func (f Fields) GetPlaceholder() string {
	size := len(f)
	var placeholders []string
	for i := 0; i < size; i++ {
		placeholders = append(placeholders, fmt.Sprintf("$%d", i+1))
	}
	return strings.Join(placeholders, ", ")
}

// GetEditedField function
func GetEditedField(s interface{}) (Fields, Values, error) {
	var fields Fields
	var values Values
	r := reflect.ValueOf(s)
	t := reflect.TypeOf(s)
	if t.Kind() != reflect.Struct {
		return nil, nil, errors.New("Not a struct")
	}

	// Iterate over all available fields and read the tag value
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		tag := field.Tag.Get("sql")
		if tag == "" {
			return nil, nil, fmt.Errorf("No sql tag for tag %v", field.Name)
		}
		v := reflect.Indirect(r).FieldByName(field.Name)
		if !v.IsNil() {
			fields = append(fields, tag)
			values = append(values, v.Interface())
		}
	}

	return fields, values, nil
}

func Round(f float64) float64 {
	return math.Floor(f + .5)
}
func RoundPlus(f float64, places int) float64 {
	shift := math.Pow(10, float64(places))
	return Round(f*shift) / shift
}

func CheckPostGisVersion(v string) bool {
	match := postgisRegexp.FindStringSubmatch(v)
	if len(match) == 0 {
		return false
	}
	version := match[1]
	segments := strings.Split(version, ".")

	if len(segments) != 3 {
		return false
	}

	major, err := strconv.Atoi(segments[0])
	if err != nil {
		return false
	}
	minor, err := strconv.Atoi(segments[1])
	if err != nil {
		return false
	}

	if major < 2 {
		return false
	}

	if minor < 4 {
		return false
	}

	return true
}
