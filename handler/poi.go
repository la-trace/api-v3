package handler

import (
	"database/sql"
	"fmt"
	"log"
	"math"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/la-trace/api-v3/api"
	"gitlab.com/la-trace/api-v3/storage"
)

func Round(f float64) float64 {
	return math.Floor(f + .5)
}
func RoundPlus(f float64, places int) float64 {
	shift := math.Pow(10, float64(places))
	return Round(f*shift) / shift
}

func (h *Handler) PatchPoi(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	p := storage.EditablePoi{}

	err := api.GetJSONContent(&p, r)

	if err != nil {
		fmt.Println(err)
		return
	}

	err = h.Dao.PatchPoi(id, p)

	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	api.JSON(w, p)
}

// GetUsers retrieve users
func (h *Handler) DeletePoi(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	err := h.Dao.DeletePoi(id)
	if err != nil {
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}
	api.JSONWithHTTPCode(w, nil, http.StatusNoContent)
}

// GetUser retrieve user
func (h *Handler) CreatePoi(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	trackID := vars["id"]

	p := &storage.Poi{}
	err := api.GetJSONContent(p, r)

	p, err = h.Dao.InsertPoi(p.Caption, trackID, p.Position.Point[0], p.Position.Point[1])

	switch {
	case err == sql.ErrNoRows:
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	case err != nil:
		log.Printf("Database error: %s", err)
		api.JSONWithHTTPCode(w, errorNotFound.payload(), errorNotFound.code)
		return
	}

	api.JSON(w, p)
}
