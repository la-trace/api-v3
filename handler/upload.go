package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/la-trace/api-v3/api"
)

func asString(p [][]float64) string {
	buf := []string{}
	for _, pt := range p {
		buf = append(buf, fmt.Sprintf("%f %f %f", pt[0], pt[1], pt[2]))
	}
	return strings.Join(buf, ",")
}

type feature struct {
	Type       string                 `json:"type"`
	Properties map[string]interface{} `json:"properties"`
	Geometry   struct {
		Type        string      `json:"type"`
		Coordinates [][]float64 `json:"coordinates"`
	} `json:"geometry"`
}

func (h *Handler) Upload(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	j := GetJWTFromContext(r)

	if _, ok := j["userId"]; !ok {
		api.JSON(w, "Error no jwt")
		return
	}

	var g feature
	err := decoder.Decode(&g)

	if err != nil {
		panic(err)
	}

	tx, err := h.DB.Begin()
	if err != nil {
		api.JSON(w, err)
		return
	}

	request := fmt.Sprintf(`
	INSERT INTO
		public.track (name, description, elevation_gain, elevation_loss, distance, user_id, rating, start_point, end_point, provider_name, provider_id, linestring)
	VALUES
		($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,'LINESTRING(%s)')
	returning id
	;`, asString(g.Geometry.Coordinates))

	stmt, err := tx.Prepare(request)

	if err != nil {
		fmt.Println(err)
		api.JSON(w, err)
		return
	}
	defer stmt.Close()

	if err != nil {

	}

	ratingJSON, _ := json.Marshal(g.Properties["rating"])
	startJSON, _ := json.Marshal(g.Properties["startPoint"])
	endJSON, _ := json.Marshal(g.Properties["endPoint"])

	var providerName *string
	var providerID *string

	if val, ok := g.Properties["providerName"]; ok {
		if val != nil {
			// @todo check if type assertion fails
			value, ok := val.(string)
			if !ok {
			}
			providerName = &value
		}
	}

	if val, ok := g.Properties["providerId"]; ok {
		if val != nil {
			value, ok := val.(string)
			if !ok {
			}
			providerID = &value
		}
	}

	row := stmt.QueryRow(g.Properties["name"], g.Properties["description"], g.Properties["elevationGain"], 0, 0, j["userId"], ratingJSON, startJSON, endJSON, providerName, providerID)

	id := ""
	err = row.Scan(&id)

	if err != nil {
		tx.Rollback()
		api.JSON(w, err)
		fmt.Println(err)
		return
	}

	if err = tx.Commit(); err != nil {
		api.JSON(w, err)
		fmt.Println(err)
		return
	}

	api.JSON(w, map[string]string{"id": id})

}
