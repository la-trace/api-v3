package handler

import (
	"fmt"
	"io/ioutil"
	"strings"

	gpx "github.com/ptrv/go-gpx"
)

type point []float64
type points []point

func (p points) asString() string {
	buf := []string{}
	for _, pt := range p {
		buf = append(buf, fmt.Sprintf("%f %f %f", pt[0], pt[1], pt[2]))
	}
	return strings.Join(buf, ",")
}

func (h *Handler) Insert() {

	files, err := ioutil.ReadDir("/Users/alexandrejomin/Google-Drive/backup-la-trace/assets/gpx")

	if err != nil {
		fmt.Println(err)
	}

	for _, file := range files {
		if !strings.HasSuffix(file.Name(), "gpx") {
			continue
		}

		fmt.Printf("%s\n", file.Name())
		path := "/Users/alexandrejomin/Google-Drive/backup-la-trace/assets/gpx/" + file.Name()

		g, err := gpx.ParseFile(path)

		if err != nil {
			fmt.Printf("error : %s\n", err)
			continue
		}

		pts := points{}

		for _, t := range g.Tracks {
			for _, s := range t.Segments {
				for _, w := range s.Waypoints {
					pts = append(pts, point{w.Lon, w.Lat, w.Ele})
				}
			}
		}

		request := fmt.Sprintf("UPDATE public.track SET linestring='LINESTRING(%s)' WHERE file = $1", pts.asString())

		_, err = h.DB.Exec(request, file.Name())

		if err != nil {
			fmt.Printf("error : %s\n", err)
			continue
		}

		fmt.Println("import done")

	}

}
