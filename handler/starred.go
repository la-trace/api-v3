package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/la-trace/api-v3/api"
)

func (h *Handler) GetStarred(w http.ResponseWriter, r *http.Request) {
	s, err := h.Dao.GetStarredTracks()

	if err != nil {
		fmt.Println(err)
	}

	api.JSON(w, s)
}
